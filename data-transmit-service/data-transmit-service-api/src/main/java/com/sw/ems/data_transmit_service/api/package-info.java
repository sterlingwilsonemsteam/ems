/**
 *The device generates data but is not capable of storing its data.
 *
 *     - provide a MQTT interface (The service could use an MQTT broker under the covers)
 *     - connect via RESTFul API in different languages
 *     - what clients can be supported in terms of language (Java (Http Client, gRPC client)
 *
 *     - the proxy client runs on a machine locally or in the same subnet, easily reachable by the device.
 *
 *     - have the ability to store the data at the client proxy then transmit to cloud when available.
 *
 *     - have the ability to have the device push data to the client (temp store if needed) then send to the cloud (typical)
 *
 *     - have the ability to poll the device at some frequency, then cache and transmit data. (useful for on demand requests)
 *         - Have a pull adapter that can invoke RESTFul API, MQTT or Java/Python client
 */
package com.sw.ems.data_transmit_service.api;