package com.sw.virtualization.v1.impl;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

/**
 * This is the domain model representation of Virtual Device.
 * 
 * @author Sudhanshu Mall
 * @version 1.0
 * @since 1.0
 *
 */

public class Dataset1 {
	

	private DeviceInfo deviceInfo;
	private List<AITagList> AItagList;
	private List<DITagList> DItagList;
	

	/**
	 * Dataset1 Constructor.
	 * 
	 * @param builder
	 */
	
	public Dataset1(Dataset1Builder builder) {
		this.deviceInfo = builder.deviceInfo;
		this.AItagList = builder.AItagList;
		this.DItagList = builder.DItagList;
		
	}

	/**
	 * Gets the deviceInfo
	 * 
	 * @return the deviceInfo
	 */
	
	public DeviceInfo getDeviceInfo() {
		return deviceInfo;
	}

	/**
	 * Gets the AITagList
	 * 
	 * @return the AITagList
	 */
	
	public List<AITagList> getAItagList() {
		return AItagList;
	}

	/**
	 * Gets the DITagList
	 * 
	 * @return the DITagList
	 */
	
	public List<DITagList> getDItagList() {
		return DItagList;
	}
	
	
	/**
	 * The toString method.
	 * 
	 */
	
	@Override
	public String toString() {
		return "Dataset1 [deviceInfo=" + deviceInfo + ", AItagList=" + AItagList + ", DItagList=" + DItagList + "]";
	}
	
	/**
	 * The hashCode method.
	 *
	 */
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((AItagList == null) ? 0 : AItagList.hashCode());
		result = prime * result + ((DItagList == null) ? 0 : DItagList.hashCode());
		result = prime * result + ((deviceInfo == null) ? 0 : deviceInfo.hashCode());
		return result;
	}

	/**
	 * The equals method.
	 * 
	 */
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dataset1Builder other = (Dataset1Builder) obj;
		if (AItagList == null) {
			if (other.AItagList != null)
				return false;
		} else if (!AItagList.equals(other.AItagList))
			return false;
		if (DItagList == null) {
			if (other.DItagList != null)
				return false;
		} else if (!DItagList.equals(other.DItagList))
			return false;
		if (deviceInfo == null) {
			if (other.deviceInfo != null)
				return false;
		} else if (!deviceInfo.equals(other.deviceInfo))
			return false;
		return true;
	}


	/**
	 * The Builder class to instantiate the Dataset1 model object.
	 * 
	 * @author Sudhanshu Mall
	 *
	 */
	
	public static class Dataset1Builder {
		
		private DeviceInfo deviceInfo;
		private List<AITagList> AItagList;
		private List<DITagList> DItagList;

		
		/**
		 * Sets the deviceInfo with builder class
		 * 
		 * @param deviceInfo the deviceInfo to set
		 */
		
		public Dataset1Builder setDeviceInfo(DeviceInfo deviceInfo) {
			this.deviceInfo = deviceInfo;
			return this;
		}
		

		/**
		 * Sets the AItagList with builder class
		 * 
		 * @param AItagList the AItagList to set
		 */

		public Dataset1Builder setAITagList(List<AITagList> AItagList) {
			this.AItagList = AItagList;
			return this;
		}
		
		/**
		 * Sets the DItagList with builder class
		 * 
		 * @param DItagList the DItagList to set
		 */

		public Dataset1Builder setDITagList(List<DITagList> DItagList) {
			this.DItagList = DItagList;
			return this;
		}
		
		/**
		 * The Dataset1 object instance set by builder class.
		 * 
		 * @return the Dataset1 Instance
		 */
		
		public Dataset1 build() {
			return new Dataset1(this);
		}
		
	}

}