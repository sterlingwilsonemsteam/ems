package com.sw.virtualization.v1.impl;

/**
 * This is the domain model representation of Virtual Device.
 * 
 * @author Sudhanshu Mall
 * @version 1.0
 * @since 1.0
 *
 */

public class PcsInverterVirtualDevice {

	private Dataset1 dataset1;

	/**
	 * The Virtual Device Constructor.
	 * 
	 * @param builder
	 */

	public PcsInverterVirtualDevice(PcsInverterVirtualDeviceBuilder builder) {
		this.dataset1 = builder.dataset1;

	}

	/**
	 * Gets the dataset1
	 * 
	 * @return the dataset1
	 */

	public Dataset1 getDataset1() {
		return dataset1;
	}

	/**
	 * The toString method.
	 * 
	 */

	@Override
	public String toString() {
		return "VirtualDeviceBuilder [dataset1=" + dataset1 + "]";
	}

	
	/**
	 * The hashCode method.
	 *
	 */

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataset1 == null) ? 0 : dataset1.hashCode());
		return result;
	}

	/**
	 * The equals method.
	 * 
	 */

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PcsInverterVirtualDeviceBuilder other = (PcsInverterVirtualDeviceBuilder) obj;
		if (dataset1 == null) {
			if (other.dataset1 != null)
				return false;
		} else if (!dataset1.equals(other.dataset1))
			return false;
		return true;
	}
	
	/**
	 * The Builder class to instantiate the VirtualDevice model object.
	 * 
	 * @author Sudhanshu Mall
	 *
	 */

	public static class PcsInverterVirtualDeviceBuilder {

		private Dataset1 dataset1;

		/**
		 * Sets the dataset1 with builder class
		 * 
		 * @param dataset1 the dataset1 to set
		 */
		public PcsInverterVirtualDeviceBuilder setDataset1(Dataset1 dataset1) {
			this.dataset1 = dataset1;
			return this;
		}

		/**
		 * The Virtual Device object instance set by builder class.
		 * 
		 * @return the Virtual Device Instance
		 */

		public PcsInverterVirtualDevice build() {
			return new PcsInverterVirtualDevice(this);
		}
	}

}
