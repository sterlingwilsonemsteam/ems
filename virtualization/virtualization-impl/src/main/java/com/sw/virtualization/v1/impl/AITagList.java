package com.sw.virtualization.v1.impl;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

/**
 * @author Sudhanshu Mall
 * @version 1.0
 * @since 1.0
 */

public class AITagList {

	
	private String tagId;
	private Integer modClass;
	private Integer modRegister;
	private String endianSize;
	private Integer funcCode;
	private String tagType;
	private String encoding;
	private Integer scaling;

	/**
	 * AITagList Constructor.
	 * 
	 * @param builder
	 */
	
	public AITagList(AITagListBuilder builder) {
		this.tagId = builder.tagId;
		this.modClass = builder.modClass;
		this.modRegister = builder.modRegister;
		this.endianSize = builder.endianSize;
		this.funcCode = builder.funcCode;
		this.tagType = builder.tagType;
		this.encoding = builder.encoding;
		this.scaling = builder.scaling;

	}
	
	/**
	 * Gets the tagId
	 * 
	 * @return the tagId
	 */
	
	public String getTagID() {
		return tagId;
	}

	/**
	 * Gets the modClass
	 * 
	 * @return the modClass
	 */
	
	public int getModClass() {
		return modClass;
	}
	
	/**
	 * Gets the modRegister
	 * 
	 * @return the modRegister
	 */

	public int getModRegister() {
		return modRegister;
	}
	
	/**
	 * Gets the endianSize
	 * 
	 * @return the endianSize
	 */

	public String getEndianSize() {
		return endianSize;
	}
	
	/**
	 * Gets the funcCode
	 * 
	 * @return the funcCode
	 */

	public int getFuncCode() {
		return funcCode;
	}
	
	/**
	 * Gets the tagType
	 * 
	 * @return the tagType
	 */

	public String getTagType() {
		return tagType;
	}
	
	/**
	 * Gets the encoding
	 * 
	 * @return the encoding
	 */

	public String getEncoding() {
		return encoding;
	}
	
	/**
	 * Gets the scaling
	 * 
	 * @return the scaling
	 */

	public int getScaling() {
		return scaling;
	}

	/**
	 * The toString method.
	 * 
	 */	
	
	@Override
	public String toString() {
		return "AITagList [tagId=" + tagId + ", modClass=" + modClass + ", modRegister=" + modRegister + ", endianSize="
				+ endianSize + ", funcCode=" + funcCode + ", tagType=" + tagType + ", encoding=" + encoding
				+ ", scaling=" + scaling + "]";
	}

	/**
	 * The hashCode method.
	 *
	 */
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((encoding == null) ? 0 : encoding.hashCode());
		result = prime * result + ((endianSize == null) ? 0 : endianSize.hashCode());
		result = prime * result + ((funcCode == null) ? 0 : funcCode.hashCode());
		result = prime * result + ((modClass == null) ? 0 : modClass.hashCode());
		result = prime * result + ((modRegister == null) ? 0 : modRegister.hashCode());
		result = prime * result + ((scaling == null) ? 0 : scaling.hashCode());
		result = prime * result + ((tagId == null) ? 0 : tagId.hashCode());
		result = prime * result + ((tagType == null) ? 0 : tagType.hashCode());
		return result;
	}
	
	/**
	 * The equals method.
	 * 
	 */

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AITagList other = (AITagList) obj;
		if (encoding == null) {
			if (other.encoding != null)
				return false;
		} else if (!encoding.equals(other.encoding))
			return false;
		if (endianSize == null) {
			if (other.endianSize != null)
				return false;
		} else if (!endianSize.equals(other.endianSize))
			return false;
		if (funcCode == null) {
			if (other.funcCode != null)
				return false;
		} else if (!funcCode.equals(other.funcCode))
			return false;
		if (modClass == null) {
			if (other.modClass != null)
				return false;
		} else if (!modClass.equals(other.modClass))
			return false;
		if (modRegister == null) {
			if (other.modRegister != null)
				return false;
		} else if (!modRegister.equals(other.modRegister))
			return false;
		if (scaling == null) {
			if (other.scaling != null)
				return false;
		} else if (!scaling.equals(other.scaling))
			return false;
		if (tagId == null) {
			if (other.tagId != null)
				return false;
		} else if (!tagId.equals(other.tagId))
			return false;
		if (tagType == null) {
			if (other.tagType != null)
				return false;
		} else if (!tagType.equals(other.tagType))
			return false;
		return true;
	}

	/**
	 * The Builder class to instantiate the AITagList model object.
	 * 
	 * @author Sudhanshu Mall
	 *
	 */	

	public static class AITagListBuilder {

		private String tagId;
		private Integer modClass;
		private Integer modRegister;
		private String endianSize;
		private Integer funcCode;
		private String tagType;
		private String encoding;
		private Integer scaling;
		
		/**
		 * Sets the tagId with builder class
		 * 
		 * @param tagId the tagId to set
		 */
		
		public AITagListBuilder setTagID(String tagId) {
			this.tagId = tagId;
			return this;
		}

		/**
		 * Sets the modClass with builder class
		 * 
		 * @param modClass the modClass to set
		 */
		
		public AITagListBuilder setModClass(int modClass) {
			this.modClass = modClass;
			return this;
		}
		
		/**
		 * Sets the modRegister with builder class
		 * 
		 * @param modRegister the modRegister to set
		 */

		public AITagListBuilder setModRegister(int modRegister) {
			this.modRegister = modRegister;
			return this;
		}
		
		/**
		 * Sets the endianSize with builder class
		 * 
		 * @param endianSize the endianSize to set
		 */

		public AITagListBuilder setEndianSize(String endianSize) {
			this.endianSize = endianSize;
			return this;
		}
		
		/**
		 * Sets the funcCode with builder class
		 * 
		 * @param funcCode the funcCode to set
		 */

		public AITagListBuilder setFuncCode(int funcCode) {
			this.funcCode = funcCode;
			return this;
		}
		
		/**
		 * Sets the tagType with builder class
		 * 
		 * @param tagType the tagType to set
		 */

		public AITagListBuilder setTagType(String tagType) {
			this.tagType = tagType;
			return this;
		}
		
		/**
		 * Sets the encoding with builder class
		 * 
		 * @param encoding the encoding to set
		 */

		public AITagListBuilder setEncoding(String encoding) {
			this.encoding = encoding;
			return this;
		}
		
		/**
		 * Sets the scaling with builder class
		 * 
		 * @param scaling the scaling to set
		 */

		public AITagListBuilder setScaling(int scaling) {
			this.scaling = scaling;
			return this;
		}

		/**
		 * The AITagList object instance set by builder class.
		 * 
		 * @return the AITagList Instance
		 */

		public AITagList build() {
			return new AITagList(this);
		}
	}

}