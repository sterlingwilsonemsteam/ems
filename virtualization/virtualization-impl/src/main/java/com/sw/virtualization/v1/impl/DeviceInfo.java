package com.sw.virtualization.v1.impl;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

/**
 * @author Sudhanshu Mall
 * @version 1.0
 * @since 1.0
 */
public class DeviceInfo {
		
	private int slaveId;
	private int dataSet;
	private int noOfAITags;
	private int noOfDITags;
	private int noOfAOTags;
	private int noOfDOTags;
	
	
	/**
	 * DeviceInfo Constructor.
	 * 
	 * @param builder
	 */
	
	public DeviceInfo(DeviceInfoBuilder builder) {
		this.slaveId = builder.slaveId;
		this.dataSet = builder.dataSet;
		this.noOfAITags = builder.noOfAITags;
		this.noOfDITags = builder.noOfDITags;
		this.noOfAOTags = builder.noOfAOTags;
		this.noOfDOTags = builder.noOfDOTags;
	}
	
	/**
	 * Gets the slaveId
	 * 
	 * @return the slaveId
	 */

	public int getSlaveID() {
		return slaveId;
	}

	/**
	 * Gets the dataSet
	 * 
	 * @return the dataSet
	 */
	
	public int getDataSet() {
		return dataSet;
	}
	
	/**
	 * Gets the noOfAITags
	 * 
	 * @return the noOfAITags
	 */
	
	public int getNoOfAITags() {
		return noOfAITags;
	}

	/**
	 * Gets the noOfDITags
	 * 
	 * @return the noOfDITags
	 */
	
	public int getNoOfDITags() {
		return noOfDITags;
	}
	
	/**
	 * Gets the noOfAOTags
	 * 
	 * @return the noOfAOTags
	 */

	public int getNoOfAOTags() {
		return noOfAOTags;
	}

	/**
	 * Gets the noOfDOTags
	 * 
	 * @return the noOfDOTags
	 */
	
	public int getNoOfDOTags() {
		return noOfDOTags;
	}

	
	/**
	 * The toString method.
	 * 
	 */	

	@Override
	public String toString() {
		return "DeviceInfo [slaveId=" + slaveId + ", dataSet=" + dataSet + ", noOfAITags=" + noOfAITags
				+ ", noOfDITags=" + noOfDITags + ", noOfAOTags=" + noOfAOTags + ", noOfDOTags=" + noOfDOTags + "]";
	}

	/**
	 * The hashCode method.
	 *
	 */
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + dataSet;
		result = prime * result + noOfAITags;
		result = prime * result + noOfAOTags;
		result = prime * result + noOfDITags;
		result = prime * result + noOfDOTags;
		result = prime * result + slaveId;
		return result;
	}
	
	/**
	 * The equals method.
	 * 
	 */

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeviceInfo other = (DeviceInfo) obj;
		if (dataSet != other.dataSet)
			return false;
		if (noOfAITags != other.noOfAITags)
			return false;
		if (noOfAOTags != other.noOfAOTags)
			return false;
		if (noOfDITags != other.noOfDITags)
			return false;
		if (noOfDOTags != other.noOfDOTags)
			return false;
		if (slaveId != other.slaveId)
			return false;
		return true;
	}

	/**
	 * The Builder class to instantiate the DeviceInfo model object.
	 * 
	 * @author Sudhanshu Mall
	 *
	 */
	
	public static class DeviceInfoBuilder {

		private int slaveId;
		private int dataSet;
		private int noOfAITags;
		private int noOfDITags;
		private int noOfAOTags;
		private int noOfDOTags;
		
		/**
		 * Sets the slaveId with builder class
		 * 
		 * @param slaveId the slaveId to set
		 */
		
		public DeviceInfoBuilder setSlaveID(int slaveId) {
			this.slaveId = slaveId;
			return this;
		}
		
		/**
		 * Sets the dataSet with builder class
		 * 
		 * @param dataSet the dataSet to set
		 */

		public DeviceInfoBuilder setDataSet(int dataSet) {
			this.dataSet = dataSet;
			return this;
		}
		
		/**
		 * Sets the noOfAITags with builder class
		 * 
		 * @param noOfAITags the noOfAITags to set
		 */

		public DeviceInfoBuilder setNoOfAITags(int noOfAITags) {
			this.noOfAITags = noOfAITags;
			return this;
		}
		
		/**
		 * Sets the noOfDITags with builder class
		 * 
		 * @param noOfDITags the noOfDITags to set
		 */

		public DeviceInfoBuilder setNoOfDITags(int noOfDITags) {
			this.noOfDITags = noOfDITags;
			return this;
		}
		
		/**
		 * Sets the noOfAOTags with builder class
		 * 
		 * @param noOfAOTags the noOfAOTags to set
		 */

		public DeviceInfoBuilder setNoOfAOTags(int noOfAOTags) {
			this.noOfAOTags = noOfAOTags;
			return this;
		}
		
		/**
		 * The DeviceInfo object instance set by builder class.
		 * 
		 * @return the DeviceInfo Instance
		 */
		
		public DeviceInfo build() {
			return new DeviceInfo(this);
					
		}
	}

}
