package com.sw.virtualization.v1.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.gson.Gson;
import com.sw.virtualization.v1.api.IPcsInverterVirtualDevice;

public class PcsInverterVirtualDeviceImpl implements IPcsInverterVirtualDevice {

	//final static Logger logger = LoggerFactory.getLogger(PcsInverterVirtualDeviceImpl.class);

	//final static Logger logger = Logger.getLogger(VirtualDeviceImpl.class);
	Properties config = new Properties();
	

	/**
	 * Method for loading Config File
	 */
	
	@Override
	public void loadingConfigFile() throws FileNotFoundException {
		try (InputStream inputStream = PcsInverterVirtualDeviceImpl.class.getClassLoader().getResourceAsStream("application.properties")) {

			// load the properties
			config.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}

		
		/*
		 * FileInputStream fis = new FileInputStream( System.getProperty("user.dir") +
		 * "/src/main/resources/application.properties");
		 * 
		 * try {
		 * 
		 * config.load(fis); } catch (IOException e) { // TODO Auto-generated catch
		 * block e.printStackTrace(); }
		 */
	}

	@Override
	public void parseJson() throws IOException  {
		
		System.out.println("new " + System.getProperty("user.dir"));
		ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY,true);
		Gson gson = new Gson();
		//Reader reader = null;

		String filePath = new File(System.getProperty("user.dir")).getParent();
		BufferedReader reader = null;
			
		System.out.println(filePath);

		//try (Reader reader = new FileReader(filePath + "/device-config-files/RegisterDeviceMap.json")) {
		try {
			reader = new BufferedReader(new FileReader(filePath + "/device-config-files/" + config.getProperty("PcsInverterRegisterMapFile")));
			
			PcsInverterVirtualDevice pcsInverterVirtualDevice = gson.fromJson(reader, PcsInverterVirtualDevice.class);

			String firstTagID = pcsInverterVirtualDevice.getDataset1().getAItagList().get(0).getTagID().replaceAll("[0-9]", "");
			System.out.println(firstTagID);
			
			String secondTagID = pcsInverterVirtualDevice.getDataset1().getDItagList().get(0).getTagID().replaceAll("[0-9]", "");
			System.out.println(secondTagID);

			int numberOfAITags = pcsInverterVirtualDevice.getDataset1().getDeviceInfo().getNoOfAITags();
			System.out.println("no of Ai Tags :" + numberOfAITags);

			int numberOfDITags = pcsInverterVirtualDevice.getDataset1().getDeviceInfo().getNoOfDITags();
			System.out.println("no of Ai Tags :" + numberOfDITags);

			Map<String, List<Map<String, String>>> payload = new HashMap<String, List<Map<String, String>>>();

			List<Map<String, String>> listOfMap = new LinkedList<Map<String, String>>();

			System.out.println(config.getProperty("Status"));

			for (int i = 1; i <= numberOfAITags; i++) {
				Map<String, String> aitag = new LinkedHashMap<String, String>();
				aitag.put(config.getProperty("TagName"), firstTagID + i);
				aitag.put(config.getProperty("Status"), "Normal");
				listOfMap.add(aitag);
			}

			for (int i = 1; i <= numberOfDITags; i++) {
				Map<String, String> ditag = new LinkedHashMap<String, String>();
				ditag.put(config.getProperty("TagName"), secondTagID + i);
				ditag.put(config.getProperty("Status"), "Normal");
				listOfMap.add(ditag);
			}

			payload.put(config.getProperty("Key"), listOfMap);

			ObjectWriter writer = objectMapper.writer(new DefaultPrettyPrinter());
			writer.writeValue(new File(filePath + "/generated-payload-files/" + config.getProperty("PcsInverterPayloadFile")), payload);
			System.out.println(payload);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		/*  Cleanup Here
		 *  Closing File
		 */
		finally {
			if (reader!=null) {
				reader.close();
			}
		}

	}

}
