package com.sw.virtualization.v1.impl;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

/**
 * @author Sudhanshu Mall
 * @version 1.0
 * @since 1.0
 */

public class DITagList{
	
	private String tagId;
	private Integer modClass;
	private Integer modRegister;
	private Integer bit;
	private String endianSize;
	private Integer funcCode;
	private String tagType;
	private String encoding;
	
	/**
	 * DITagList Constructor.
	 * 
	 * @param builder
	 */
	
	public DITagList(DITagListBuilder builder) {
		this.tagId=builder.tagId;
		this.modClass=builder.modClass;
		this.modRegister=builder.modRegister;
		this.bit=builder.bit;
		this.endianSize=builder.endianSize;
		this.funcCode=builder.funcCode;
		this.tagType=builder.tagType;
		this.encoding=builder.encoding;
				
	}
	
	/**
	 * Gets the tagId
	 * 
	 * @return the tagId
	 */
	
	public String getTagID() {
		return tagId;
	}
	
	/**
	 * Gets the modClass
	 * 
	 * @return the modClass
	 */
	
	public Integer getModClass() {
		return modClass;
	}
	
	/**
	 * Gets the modRegister
	 * 
	 * @return the modRegister
	 */
	
	public Integer getModRegister() {
		return modRegister;
	}
	
	/**
	 * Gets the bit
	 * 
	 * @return the bit
	 */
	
	public Integer getBit() {
		return bit;
	}
	
	/**
	 * Gets the endianSize
	 * 
	 * @return the endianSize
	 */
	
	public String getEndianSize() {
		return endianSize;
	}
	
	/**
	 * Gets the funcCode
	 * 
	 * @return the funcCode
	 */
	
	public Integer getFuncCode() {
		return funcCode;
	}
	
	/**
	 * Gets the tagType
	 * 
	 * @return the tagType
	 */
	
	public String getTagType() {
		return tagType;
	}
	
	/**
	 * Gets the encoding
	 * 
	 * @return the encoding
	 */
	
	public String getEncoding() {
		return encoding;
	}
	
	/**
	 * The toString method.
	 * 
	 */	
		
	@Override
	public String toString() {
		return "DITagList [tagId=" + tagId + ", modClass=" + modClass + ", modRegister=" + modRegister + ", bit=" + bit
				+ ", endianSize=" + endianSize + ", funcCode=" + funcCode + ", tagType=" + tagType + ", encoding="
				+ encoding + "]";
	}
		
	/**
	 * The hashCode method.
	 *
	 */
		
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bit == null) ? 0 : bit.hashCode());
		result = prime * result + ((encoding == null) ? 0 : encoding.hashCode());
		result = prime * result + ((endianSize == null) ? 0 : endianSize.hashCode());
		result = prime * result + ((funcCode == null) ? 0 : funcCode.hashCode());
		result = prime * result + ((modClass == null) ? 0 : modClass.hashCode());
		result = prime * result + ((modRegister == null) ? 0 : modRegister.hashCode());
		result = prime * result + ((tagId == null) ? 0 : tagId.hashCode());
		result = prime * result + ((tagType == null) ? 0 : tagType.hashCode());
		return result;
	}
	
	/**
	 * The equals method.
	 * 
	 */

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DITagList other = (DITagList) obj;
		if (bit == null) {
			if (other.bit != null)
				return false;
		} else if (!bit.equals(other.bit))
			return false;
		if (encoding == null) {
			if (other.encoding != null)
				return false;
		} else if (!encoding.equals(other.encoding))
			return false;
		if (endianSize == null) {
			if (other.endianSize != null)
				return false;
		} else if (!endianSize.equals(other.endianSize))
			return false;
		if (funcCode == null) {
			if (other.funcCode != null)
				return false;
		} else if (!funcCode.equals(other.funcCode))
			return false;
		if (modClass == null) {
			if (other.modClass != null)
				return false;
		} else if (!modClass.equals(other.modClass))
			return false;
		if (modRegister == null) {
			if (other.modRegister != null)
				return false;
		} else if (!modRegister.equals(other.modRegister))
			return false;
		if (tagId == null) {
			if (other.tagId != null)
				return false;
		} else if (!tagId.equals(other.tagId))
			return false;
		if (tagType == null) {
			if (other.tagType != null)
				return false;
		} else if (!tagType.equals(other.tagType))
			return false;
		return true;
	}

	/**
	 * The Builder class to instantiate the DITagList model object.
	 * 
	 * @author Sudhanshu Mall
	 *
	 */	

	public static class DITagListBuilder{
		
		private String tagId;
		private Integer modClass;
		private Integer modRegister;
		private Integer bit;
		private String endianSize;
		private Integer funcCode;
		private String tagType;
		private String encoding;
		
		/**
		 * Sets the tagId with builder class
		 * 
		 * @param tagId the tagId to set
		 */
		
		public DITagListBuilder setTagId(String tagId) {
			this.tagId=tagId;
			return this;
		}
		
		/**
		 * Sets the modClass with builder class
		 * 
		 * @param modClass the modClass to set
		 */
		
		public DITagListBuilder setModClass(int modClass) {
			this.modClass = modClass;
			return this;
		}
		
		/**
		 * Sets the modRegister with builder class
		 * 
		 * @param modRegister the modRegister to set
		 */
		
		public DITagListBuilder setModRegister(int modRegister) {
			this.modRegister = modRegister;
			return this;
		}
		
		/**
		 * Sets the bit with builder class
		 * 
		 * @param bit the bit to set
		 */
		
		public DITagListBuilder setBit(Integer bit) {
			this.bit=bit;
			return this;
		}
		
		/**
		 * Sets the endianSize with builder class
		 * 
		 * @param endianSize the endianSize to set
		 */
		
		public DITagListBuilder setEndianSize(String endianSize) {
			this.endianSize = endianSize;
			return this;
		}
		
		/**
		 * Sets the funcCode with builder class
		 * 
		 * @param funcCode the funcCode to set
		 */
		
		public DITagListBuilder setFuncCode(int funcCode) {
			this.funcCode = funcCode;
			return this;
		}
		
		/**
		 * Sets the tagType with builder class
		 * 
		 * @param tagType the tagType to set
		 */
		
		public DITagListBuilder setTagType(String tagType) {
			this.tagType = tagType;
			return this;
		}
		
		/**
		 * Sets the encoding with builder class
		 * 
		 * @param encoding the encoding to set
		 */
		
		public DITagListBuilder setEncoding(String encoding) {
			this.encoding = encoding;
			return this;
		}
		
		/**
		 * The DITagList object instance set by builder class.
		 * 
		 * @return the DITagList Instance
		 */
				
		public DITagList build() {
			return new DITagList(this);
		}
		
	}

}