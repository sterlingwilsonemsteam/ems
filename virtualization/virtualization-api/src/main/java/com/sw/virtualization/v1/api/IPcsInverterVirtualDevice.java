package com.sw.virtualization.v1.api;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface IPcsInverterVirtualDevice {
	
	public void loadingConfigFile() throws FileNotFoundException;
	
	public void parseJson() throws IOException;
	
}