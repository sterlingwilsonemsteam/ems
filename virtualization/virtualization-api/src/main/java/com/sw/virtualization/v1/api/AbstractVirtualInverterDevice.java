package com.sw.virtualization.v1.api;

import com.sw.ems.device.driver.api.IInverterDeviceDriver;

public abstract class AbstractVirtualInverterDevice extends AbstractVirtualDevice implements IInverterDeviceDriver {
}
