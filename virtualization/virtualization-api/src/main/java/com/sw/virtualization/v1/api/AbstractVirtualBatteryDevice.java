package com.sw.virtualization.v1.api;

import com.sw.ems.device.driver.api.config.IDeviceConfiguration;

public class AbstractVirtualBatteryDevice extends AbstractVirtualDevice implements IVirtualBatteryDevice {

    @Override
    public IDeviceConfiguration getConfiguration() {
        return null;
    }
}
