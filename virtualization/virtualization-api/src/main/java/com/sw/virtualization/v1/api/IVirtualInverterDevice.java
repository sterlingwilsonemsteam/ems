package com.sw.virtualization.v1.api;

import com.sw.ems.device.driver.api.IInverterDeviceDriver;

public interface IVirtualInverterDevice extends IInverterDeviceDriver {
}
