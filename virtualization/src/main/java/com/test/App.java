package com.test;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sw.virtualization.v1.api.IPcsInverterVirtualDevice;
import com.sw.virtualization.v1.impl.PcsInverterVirtualDeviceImpl;

public class App {

	//final static Logger logger = LoggerFactory.getLogger(App.class);
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		//logger.info("============ Main Started ============");
		IPcsInverterVirtualDevice iPcsInverterVirtualDevice =  new PcsInverterVirtualDeviceImpl();
		//logger.info("Going to Call loadingConfigFile() method");
		iPcsInverterVirtualDevice.loadingConfigFile();
		//logger.info("Going to call parseJson() method");
		iPcsInverterVirtualDevice.parseJson();
		//logger.info("============ Main Ended ============");
		/*
		 * if(logger.isDebugEnabled()){ logger.debug("Main End"); }
		 */
	}

}
