package com.test;

import static org.testng.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.google.gson.Gson;
import com.sw.virtualization.v1.impl.AITagList;
import com.sw.virtualization.v1.impl.DITagList;
import com.sw.virtualization.v1.impl.PcsInverterVirtualDevice;
import com.sw.virtualization.v1.impl.PcsInverterVirtualDeviceImpl;

public class PcsInverterVirtualDeviceTestCases {
	
	Properties config = new Properties();
	Gson gson = new Gson();
	String filePath = new File(System.getProperty("user.dir")).getParent();
	BufferedReader reader = null;
	
	
	@BeforeTest
	public void loadPropertiesFile() {
		
		try (InputStream inputStream = PcsInverterVirtualDeviceImpl.class.getClassLoader().getResourceAsStream("application.properties")) {

			// load the properties
			config.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
		
	/**
	 *  Unit Test to check if PcsInverterRegisterMapFile file exists
	 */
	@Test
	public void fileExists() {
		
		
		File file = new File(filePath + "/device-config-files/"+ config.getProperty("PcsInverterRegisterMapFile"));
		assertTrue(file.exists());
	}
	

	@Test
	public void checkAITagListNotEmpty() throws IOException {
		
				
		reader = new BufferedReader(new FileReader(filePath + "/device-config-files/" + config.getProperty("PcsInverterRegisterMapFile")));
		
		
		PcsInverterVirtualDevice pcsInverterVirtualDevice = gson.fromJson(reader, PcsInverterVirtualDevice.class);
		
		AITagList firstTag = pcsInverterVirtualDevice.getDataset1().getAItagList().get(0);
		
		System.out.println(pcsInverterVirtualDevice.getDataset1().getAItagList().get(0));
		
		//assertEquals(pcsInverterVirtualDevice.getDataset1().getAItagList().get(0).getTagID().replaceAll("[0-9]", ""), "AIid1");
			
		assertNotNull(firstTag);
		
	}
	

	@Test
	public void checkDITagListNotEmpty() throws IOException {
		
		
		reader = new BufferedReader(new FileReader(filePath + "/device-config-files/" + config.getProperty("PcsInverterRegisterMapFile")));
				
		PcsInverterVirtualDevice pcsInverterVirtualDevice = gson.fromJson(reader, PcsInverterVirtualDevice.class);
		
		DITagList secondTag = pcsInverterVirtualDevice.getDataset1().getDItagList().get(0);
		
		System.out.println(pcsInverterVirtualDevice.getDataset1().getDItagList().get(0));
		
		//assertEquals(pcsInverterVirtualDevice.getDataset1().getAItagList().get(0).getTagID().replaceAll("[0-9]", ""), "AIid1");
			
		assertNotNull(secondTag);
		
	}
	
	
	@Test
	public void AITagNotEmpty() throws FileNotFoundException {
		
		
		reader = new BufferedReader(new FileReader(filePath + "/device-config-files/" + config.getProperty("PcsInverterRegisterMapFile")));
		
		PcsInverterVirtualDevice pcsInverterVirtualDevice = gson.fromJson(reader, PcsInverterVirtualDevice.class);

		int numberOfAITags = pcsInverterVirtualDevice.getDataset1().getDeviceInfo().getNoOfAITags();
		
		System.out.println(numberOfAITags);
		
		assertNotNull(numberOfAITags);
		
	}
	
	@Test
	public void DITagNotEmpty() throws FileNotFoundException {
		
	
		reader = new BufferedReader(new FileReader(filePath + "/device-config-files/" + config.getProperty("PcsInverterRegisterMapFile")));
		
		PcsInverterVirtualDevice pcsInverterVirtualDevice = gson.fromJson(reader, PcsInverterVirtualDevice.class);

		int numberOfDITags = pcsInverterVirtualDevice.getDataset1().getDeviceInfo().getNoOfDITags();
		
		System.out.println(numberOfDITags);
		
		assertNotNull(numberOfDITags);
		
	}
	
	
	@Test
	public void CheckNoOfTagsWithPayloadJson() throws FileNotFoundException {
		
		
		reader = new BufferedReader(new FileReader(filePath + "/device-config-files/" + config.getProperty("PcsInverterRegisterMapFile")));
		
		PcsInverterVirtualDevice pcsInverterVirtualDevice = gson.fromJson(reader, PcsInverterVirtualDevice.class);

		int numberOfAITags = pcsInverterVirtualDevice.getDataset1().getDeviceInfo().getNoOfAITags();

		int numberOfDITags = pcsInverterVirtualDevice.getDataset1().getDeviceInfo().getNoOfDITags();
		
		int totalNumberOfTags = numberOfAITags + numberOfDITags;
				
		System.out.println("Total Tags - "+totalNumberOfTags);
		
		
				
	}
	
	
}
