package com.sw.ems.device.common.controller;

import com.sw.ems.device.common.IAssetController;

//TODO Note - Not all devices have a mapping
/**
 *
 */
public interface IDeviceController extends IAssetController {
    IDeviceControllerConfiguration getConfiguration();	
}
