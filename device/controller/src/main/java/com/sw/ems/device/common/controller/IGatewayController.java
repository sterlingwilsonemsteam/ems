package com.sw.ems.device.common.controller;

import com.sw.ems.device.common.IAssetController;

/**
 * TODOs
 *   - Manages device controllers
 */
public interface IGatewayController extends IAssetController {
    IGatewayControllerConfiguration getConfiguration();
}
