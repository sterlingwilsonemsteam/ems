package com.sw.ems.device.driver.impl;

import com.sw.ems.device.driver.api.data.IDeviceDataPayload;
import com.sw.ems.device.driver.api.protocol.IEMSDeviceCommunicationProtocol;

public abstract class AbstractEMSDeviceProtocolAdapter implements IEMSDeviceCommunicationProtocol {

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendData(IDeviceDataPayload datapayload) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IDeviceDataPayload requestData() {
        return null;
    }
}
