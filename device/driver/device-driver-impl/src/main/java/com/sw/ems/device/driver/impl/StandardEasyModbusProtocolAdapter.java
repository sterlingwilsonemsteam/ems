package com.sw.ems.device.driver.impl;

import com.sw.ems.device.driver.api.data.IDeviceDataPayload;

import java.io.IOException;
import java.net.UnknownHostException;

/**
 *
 */
public final class StandardEasyModbusProtocolAdapter extends AbstractEMSModbusProtocolAdapter {

    /**
     * Constructor.
     *
     * @param dataset dataset
     */
    public StandardEasyModbusProtocolAdapter(int dataset) {
        super(dataset);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendData(IDeviceDataPayload datapayload) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IDeviceDataPayload requestData() {
        return null;
    }

    //2dl here use single ton design pattern
    public StandardEasyModbusProtocolAdapter modbusDeviceConnection() throws UnknownHostException, IOException
    {
        StandardEasyModbusProtocolAdapter modbusClient = new StandardEasyModbusProtocolAdapter(0);//2dl need to check this code
        //TODO modbusClient.Connect(targetIP,port);
        //TODO modbusClient.setUnitIdentifier((byte)slaveId);
        return (modbusClient);

    }

}
