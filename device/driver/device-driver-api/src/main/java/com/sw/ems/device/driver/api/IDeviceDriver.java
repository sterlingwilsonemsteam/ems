package com.sw.ems.device.driver.api;

import com.sw.ems.device.driver.api.config.IDeviceConfiguration;

/**
 *
 */
public interface IDeviceDriver {
    IDeviceConfiguration getConfiguration();
}
