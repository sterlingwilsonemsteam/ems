package com.sw.ems.device.driver.api.protocol;

import com.sw.ems.device.driver.api.data.IDeviceDataPayload;

public interface IEMSDeviceCommunicationProtocol {
    /**
     *
     * @param datapayload
     * @throws EMSDeviceProtocolException
     */
    void sendData(IDeviceDataPayload datapayload) throws EMSDeviceProtocolException;

    /**
     *
     * @return
     * @throws EMSDeviceProtocolException
     */
    IDeviceDataPayload requestData() throws EMSDeviceProtocolException;
}
