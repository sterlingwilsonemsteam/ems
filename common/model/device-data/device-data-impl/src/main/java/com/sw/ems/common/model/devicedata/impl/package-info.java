/**
 * Data adapters to transform device data attributes to the standard domain model.
 */
package com.sw.ems.common.model.devicedata.impl;