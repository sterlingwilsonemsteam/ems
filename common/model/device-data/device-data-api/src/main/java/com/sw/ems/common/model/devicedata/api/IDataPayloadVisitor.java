package com.sw.ems.common.model.devicedata.api;

/**
 *
 */
public interface IDataPayloadVisitor {
    void visit(IDataPayload dataPayload);
}
