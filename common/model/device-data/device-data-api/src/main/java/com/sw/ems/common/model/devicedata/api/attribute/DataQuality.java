package com.sw.ems.common.model.devicedata.api.attribute;

public enum DataQuality {
    VALID,
    NOT_VERIFIED,
    NOT_PRESENT,
    PRESENT_CORRUPTED;
}
