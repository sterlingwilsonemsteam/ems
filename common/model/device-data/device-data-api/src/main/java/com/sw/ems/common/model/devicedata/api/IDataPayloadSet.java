package com.sw.ems.common.model.devicedata.api;

import java.util.List;

/**
 *
 */
public interface IDataPayloadSet<T> {
    List<IDataPayload<T>> getDataPayloads();
    //TODO Possibly create other operations to navigate through the payloads
}
