package com.sw.ems.common.model.devicedata.api;

/**
 *
 */
//TODO Visitor pattern for data transformations
public interface IDataTransformation<T> extends IDataPayloadVisitor {
    void accept(IDataPayload<T> dataPayload);
}
