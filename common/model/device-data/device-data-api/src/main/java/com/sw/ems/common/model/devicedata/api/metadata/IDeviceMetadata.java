package com.sw.ems.common.model.devicedata.api.metadata;

import java.time.Instant;

public interface IDeviceMetadata {
    String getId();
    String getMetadataVersion();
    Instant getDate();
}
