package com.sw.ems.common.model.devicedata.api;

import java.time.Instant;

/**
 *
 */
public interface IDeviceData<T> extends IDataPayload<T> {
    Instant getTime();
    //TODO Tie this to the JSON schema
    void visit(IDataPayloadVisitor visitor);
}
