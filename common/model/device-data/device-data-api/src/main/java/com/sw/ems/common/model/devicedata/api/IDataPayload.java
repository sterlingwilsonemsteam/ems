package com.sw.ems.common.model.devicedata.api;

import java.time.Instant;

/**
 * Wrapper around a data payload of any type.
 *
 * @param <T>
 */
public interface IDataPayload<T> {
    Instant getTime();
    //TODO Tie this to the JSON schema
    void visit(IDataPayloadVisitor visitor);
}
