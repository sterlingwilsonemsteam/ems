package com.sw.ems.common.model.devicedata.api;

import java.time.Instant;

/**
 *
 */
public interface IAggregateDataPayload {
    /**
     *
     * @return start time
     */
    Instant startTime();

    /**
     *
     * @return end time
     */
    Instant endTime();
}
