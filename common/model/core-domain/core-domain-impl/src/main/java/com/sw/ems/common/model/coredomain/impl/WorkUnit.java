/**
 * 
 */
package com.sw.ems.common.model.coredomain.impl;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import com.sw.ems.common.model.coredomain.impl.device.Device;

//TODO Move this
/**
 * @author hema
 *
 */
public class WorkUnit extends Device {
	
	private static final long serialVersionUID = 1L;
	private String id;
    private String definition;

    public WorkUnit() {
        //for Spring-Web binding
    }
    @JsonCreator
    public WorkUnit(@JsonProperty("id") String id,
                    @JsonProperty("definition") String definition) {
        this.id = id;
        this.definition = definition;
    }

    public String getId() {
        return id;
    }


    public String getDefinition() {
        return definition;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("definition", definition)
                .toString();
    }
}


