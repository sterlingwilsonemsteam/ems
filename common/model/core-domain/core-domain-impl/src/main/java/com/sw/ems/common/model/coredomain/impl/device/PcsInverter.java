package com.sw.ems.common.model.coredomain.impl.device;

import java.math.BigInteger;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.sw.ems.common.model.coredomain.api.device.EmergencySwitchStatusEnum;
import com.sw.ems.common.model.coredomain.api.device.inverter.IPcsInverter;
import com.sw.ems.common.model.coredomain.api.device.PvInverterTypeEnum;
import com.sw.ems.common.model.coredomain.api.device.RunningStatusEnum;

/** This is the domain model representation of PcsInverter. 
 *  
 * @author Gaurav Garg
 * @version 1.0
 * @since 1.0
 *
 */
@JsonDeserialize(builder = PcsInverter.PcsInverterBuilder.class)
public final class PcsInverter extends Device implements IPcsInverter {

	/**
	 *  The serial version UID.
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private BigInteger powerOutputAC;
	private BigInteger pcsVoltageAC;
	private BigInteger pcsCurrentAC;
	private BigInteger powerActiveAC;
	private BigInteger powerReactiveAC;
	private BigInteger powerApparentAC;
	private BigInteger dcBusVoltage;
	private BigInteger frequencyGrid;
	private Long pcsStatus; // for UInt16 we are taking long because range would be more
	private RunningStatusEnum runningStatus;
	private BigInteger powerBattery;
	private Long watchdogTimer;
	private Long minorAlarm;
	private Long majorAlarm;
	private EmergencySwitchStatusEnum emergencySwitchStatus;
	private Long currentBattery;
	private BigInteger tempBattery1;
	private BigInteger tempBattery2;
	private BigInteger tempBattery3;
	private BigInteger tempBatteryMin;
	private BigInteger tempBatteryMax;
	private BigInteger tempBatteryAvg;
	private BigInteger battSoC;
	private BigInteger energyHourLast;
	private BigInteger energyDayLast;
	private BigInteger energyMonthLast;
	private PvInverterTypeEnum pvInverterType;
	
	/** The PcsInverter Constructor.
	 * @param builder
	 */
	public PcsInverter(PcsInverterBuilder builder) {
		this.id = builder.id;
		this.deviceId = builder.deviceId;
		this.serialNumber = builder.serialNumber;
		this.manufacturer = builder.manufacturer;
		this.modelNumber = builder.modelNumber;
		this.fwVersion = builder.fwVersion;
		this.pvInverterType = builder.pvInverterType;
		this.size = builder.size;
		this.powerOutputAC = builder.powerOutputAC;
		this.pcsVoltageAC = builder.pcsVoltageAC;
		this.pcsCurrentAC = builder.pcsCurrentAC;
		this.powerActiveAC = builder.powerActiveAC;
		this.powerReactiveAC = builder.powerReactiveAC;
		this.powerApparentAC = builder.powerApparentAC;
		this.dcBusVoltage = builder.dcBusVoltage;
		this.frequencyGrid = builder.frequencyGrid;
		this.pcsStatus = builder.pcsStatus;
		this.runningStatus = builder.runningStatus;
		this.powerBattery = builder.powerBattery;
		this.watchdogTimer = builder.watchdogTimer;
		this.minorAlarm = builder.minorAlarm;
		this.majorAlarm = builder.majorAlarm;
		this.emergencySwitchStatus = builder.emergencySwitchStatus;
		this.currentBattery = builder.currentBattery;
		this.tempBattery1 = builder.tempBattery1;
		this.tempBattery2 = builder.tempBattery2;
		this.tempBattery3 = builder.tempBattery3;
		this.tempBatteryMin = builder.tempBatteryMin;
		this.tempBatteryMax = builder.tempBatteryMax;
		this.tempBatteryAvg = builder.tempBatteryAvg;
		this.battSoC = builder.battSoC;
		this.energyHourLast = builder.energyHourLast;
		this.energyDayLast = builder.energyDayLast;
		this.energyMonthLast = builder.energyMonthLast;
	}


	/** Gets the id.
	 * @return the id.
	 */
	public Long getId() {
		return id;
	}


	/** Gets the deviceId.	
	 * @return the deviceId.
	 */
	public String getDeviceId() {
		return deviceId;
	}


	/** Gets the serialNumber.
	 * @return the serialNumber.
	 */
	public String getSerialNumber() {
		return serialNumber;
	}


	/** Gets the manufacturer.
	 * @return the manufacturer.
	 */
	public String getManufacturer() {
		return manufacturer;
	}


	/** Gets the modelNumber.
	 * @return the modelNumber.
	 */
	public String getModelNumber() {
		return modelNumber;
	}


	/**Gets the fwVersion.
	 * @return the fwVersion.
	 */
	public String getFwVersion() {
		return fwVersion;
	}


	/**Gets the pvInverterType.
	 * @return the pvInverterType.
	 */
	public PvInverterTypeEnum getPvInverterType() {
		return pvInverterType;
	}


	/** Gets the size.
	 * @return the size.
	 */
	public String getSize() {
		return size;
	}


	/** Gets the powerOutputAC.
	 * @return the powerOutputAC.
	 */
	public BigInteger getPowerOutputAC() {
		return powerOutputAC;
	}


	/** Gets the pcsVoltageAC.
	 * @return the pcsVoltageAC.
	 */
	public BigInteger getPcsVoltageAC() {
		return pcsVoltageAC;
	}


	/** Gets the pcsCurrentAC.
	 * @return the pcsCurrentAC.
	 */
	public BigInteger getPcsCurrentAC() {
		return pcsCurrentAC;
	}


	/** Gets the powerActiveAC.
	 * @return the powerActiveAC.
	 */
	public BigInteger getPowerActiveAC() {
		return powerActiveAC;
	}


	/** Gets the powerReactiveAC. 
	 * @return the powerReactiveAC.
	 */
	public BigInteger getPowerReactiveAC() {
		return powerReactiveAC;
	}


	/** Gets the powerApparentAC.
	 * @return the powerApparentAC.
	 */
	public BigInteger getPowerApparentAC() {
		return powerApparentAC;
	}


	/** Gets the dcBusVoltage.
	 * @return the dcBusVoltage.
	 */
	public BigInteger getDcBusVoltage() {
		return dcBusVoltage;
	}


	/** Gets the frequencyGrid.
	 * @return the frequencyGrid.
	 */
	public BigInteger getFrequencyGrid() {
		return frequencyGrid;
	}


	/** Gets the pcsStatus.
	 * @return the pcsStatus.
	 */
	public Long getPcsStatus() {
		return pcsStatus;
	}


	/** Gets the runningStatus.
	 * @return the runningStatus.
	 */
	public RunningStatusEnum getRunningStatus() {
		return runningStatus;
	}


	/** Gets the powerBattery.
	 * @return the powerBattery.
	 */
	public BigInteger getPowerBattery() {
		return powerBattery;
	}


	/** Gets the watchdogTimer.
	 * @return the watchdogTimer.
	 */
	public Long getWatchdogTimer() {
		return watchdogTimer;
	}


	/** Gets the minorAlarm.
	 * @return the minorAlarm.
	 */
	public Long getMinorAlarm() {
		return minorAlarm;
	}


	/** Gets the majorAlarm.
	 * @return the majorAlarm.
	 */
	public Long getMajorAlarm() {
		return majorAlarm;
	}


	/** Gets the emergencySwitchStatus.
	 * @return the emergencySwitchStatus.
	 */
	public EmergencySwitchStatusEnum getEmergencySwitchStatus() {
		return emergencySwitchStatus;
	}


	/** Gets the currentBattery.
	 * @return the currentBattery.
	 */
	public Long getCurrentBattery() {
		return currentBattery;
	}


	/** Gets the tempBattery1.
	 * @return the tempBattery1.
	 */
	public BigInteger getTempBattery1() {
		return tempBattery1;
	}


	/** Gets the tempBattery2.
	 * @return the tempBattery2.
	 */
	public BigInteger getTempBattery2() {
		return tempBattery2;
	}


	/** Gets the tempBattery3.
	 * @return the tempBattery3.
	 */
	public BigInteger getTempBattery3() {
		return tempBattery3;
	}


	/** Gets the tempBatteryMin.
	 * @return the tempBatteryMin.
	 */
	public BigInteger getTempBatteryMin() {
		return tempBatteryMin;
	}


	/** Gets the tempBatteryMax.
	 * @return the tempBatteryMax.
	 */
	public BigInteger getTempBatteryMax() {
		return tempBatteryMax;
	}


	/** Gets the tempBatteryAvg.
	 * @return the tempBatteryAvg.
	 */
	public BigInteger getTempBatteryAvg() {
		return tempBatteryAvg;
	}


	/** Gets the battSoC.
	 * @return the battSoC.
	 */
	public BigInteger getBattSoC() {
		return battSoC;
	}


	/** Gets the energyHourLast.
	 * @return the energyHourLast.
	 */
	public BigInteger getEnergyHourLast() {
		return energyHourLast;
	}


	/** Gets the energyDayLast.
	 * @return the energyDayLast.
	 */
	public BigInteger getEnergyDayLast() {
		return energyDayLast;
	}

	/** Gets the energyMonthLast.
	 * @return the energyMonthLast.
	 */
	public BigInteger getEnergyMonthLast() {
		return energyMonthLast;
	}

	/** The toString method.
	 * 
	 */
	@Override
	public String toString() {
		return "PcsInverter [deviceId=" + deviceId + ", serialNumber=" + serialNumber + ", manufacturer=" + manufacturer
				+ ", modelNumber=" + modelNumber + ", fwVersion=" + fwVersion + ", pvInverterType=" + pvInverterType
				+ ", size=" + size + ", powerOutputAC=" + powerOutputAC + ", pcsVoltageAC=" + pcsVoltageAC
				+ ", pcsCurrentAC=" + pcsCurrentAC + ", powerActiveAC=" + powerActiveAC + ", powerReactiveAC="
				+ powerReactiveAC + ", powerApparentAC=" + powerApparentAC + ", dcBusVoltage=" + dcBusVoltage
				+ ", frequencyGrid=" + frequencyGrid + ", pcsStatus=" + pcsStatus + ", runningStatus=" + runningStatus
				+ ", powerBattery=" + powerBattery + ", watchdogTimer=" + watchdogTimer + ", minorAlarm=" + minorAlarm
				+ ", majorAlarm=" + majorAlarm + ", emergencySwitchStatus=" + emergencySwitchStatus
				+ ", currentBattery=" + currentBattery + ", tempBattery1=" + tempBattery1 + ", tempBattery2="
				+ tempBattery2 + ", tempBattery3=" + tempBattery3 + ", tempBatteryMin=" + tempBatteryMin
				+ ", tempBatteryMax=" + tempBatteryMax + ", tempBatteryAvg=" + tempBatteryAvg + ", battSoC=" + battSoC
				+ ", energyHourLast=" + energyHourLast + ", energyDayLast=" + energyDayLast + ", energyMonthLast="
				+ energyMonthLast + "]";
	}
	
	/** The hashCode method.
	 *
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currentBattery == null) ? 0 : currentBattery.hashCode());
		result = prime * result + ((battSoC == null) ? 0 : battSoC.hashCode());
		result = prime * result + ((dcBusVoltage == null) ? 0 : dcBusVoltage.hashCode());
		result = prime * result + ((deviceId == null) ? 0 : deviceId.hashCode());
		result = prime * result + ((emergencySwitchStatus == null) ? 0 : emergencySwitchStatus.hashCode());
		result = prime * result + ((energyDayLast == null) ? 0 : energyDayLast.hashCode());
		result = prime * result + ((energyHourLast == null) ? 0 : energyHourLast.hashCode());
		result = prime * result + ((energyMonthLast == null) ? 0 : energyMonthLast.hashCode());
		result = prime * result + ((frequencyGrid == null) ? 0 : frequencyGrid.hashCode());
		result = prime * result + ((fwVersion == null) ? 0 : fwVersion.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((majorAlarm == null) ? 0 : majorAlarm.hashCode());
		result = prime * result + ((manufacturer == null) ? 0 : manufacturer.hashCode());
		result = prime * result + ((minorAlarm == null) ? 0 : minorAlarm.hashCode());
		result = prime * result + ((modelNumber == null) ? 0 : modelNumber.hashCode());
		result = prime * result + ((pcsCurrentAC == null) ? 0 : pcsCurrentAC.hashCode());
		result = prime * result + ((pcsStatus == null) ? 0 : pcsStatus.hashCode());
		result = prime * result + ((pcsVoltageAC == null) ? 0 : pcsVoltageAC.hashCode());
		result = prime * result + ((powerActiveAC == null) ? 0 : powerActiveAC.hashCode());
		result = prime * result + ((powerApparentAC == null) ? 0 : powerApparentAC.hashCode());
		result = prime * result + ((powerBattery == null) ? 0 : powerBattery.hashCode());
		result = prime * result + ((powerOutputAC == null) ? 0 : powerOutputAC.hashCode());
		result = prime * result + ((powerReactiveAC == null) ? 0 : powerReactiveAC.hashCode());
		result = prime * result + ((pvInverterType == null) ? 0 : pvInverterType.hashCode());
		result = prime * result + ((runningStatus == null) ? 0 : runningStatus.hashCode());
		result = prime * result + ((serialNumber == null) ? 0 : serialNumber.hashCode());
		result = prime * result + ((size == null) ? 0 : size.hashCode());
		result = prime * result + ((tempBattery1 == null) ? 0 : tempBattery1.hashCode());
		result = prime * result + ((tempBattery2 == null) ? 0 : tempBattery2.hashCode());
		result = prime * result + ((tempBattery3 == null) ? 0 : tempBattery3.hashCode());
		result = prime * result + ((tempBatteryAvg == null) ? 0 : tempBatteryAvg.hashCode());
		result = prime * result + ((tempBatteryMax == null) ? 0 : tempBatteryMax.hashCode());
		result = prime * result + ((tempBatteryMin == null) ? 0 : tempBatteryMin.hashCode());
		result = prime * result + ((watchdogTimer == null) ? 0 : watchdogTimer.hashCode());
		return result;
	}

	/** The equals method.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof PcsInverter))
			return false;
		PcsInverter other = (PcsInverter) obj;
		if (currentBattery == null) {
			if (other.currentBattery != null)
				return false;
		} else if (!currentBattery.equals(other.currentBattery))
			return false;
		if (battSoC == null) {
			if (other.battSoC != null)
				return false;
		} else if (!battSoC.equals(other.battSoC))
			return false;
		if (dcBusVoltage == null) {
			if (other.dcBusVoltage != null)
				return false;
		} else if (!dcBusVoltage.equals(other.dcBusVoltage))
			return false;
		if (deviceId == null) {
			if (other.deviceId != null)
				return false;
		} else if (!deviceId.equals(other.deviceId))
			return false;
		if (emergencySwitchStatus != other.emergencySwitchStatus)
			return false;
		if (energyDayLast == null) {
			if (other.energyDayLast != null)
				return false;
		} else if (!energyDayLast.equals(other.energyDayLast))
			return false;
		if (energyHourLast == null) {
			if (other.energyHourLast != null)
				return false;
		} else if (!energyHourLast.equals(other.energyHourLast))
			return false;
		if (energyMonthLast == null) {
			if (other.energyMonthLast != null)
				return false;
		} else if (!energyMonthLast.equals(other.energyMonthLast))
			return false;
		if (frequencyGrid == null) {
			if (other.frequencyGrid != null)
				return false;
		} else if (!frequencyGrid.equals(other.frequencyGrid))
			return false;
		if (fwVersion == null) {
			if (other.fwVersion != null)
				return false;
		} else if (!fwVersion.equals(other.fwVersion))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (majorAlarm == null) {
			if (other.majorAlarm != null)
				return false;
		} else if (!majorAlarm.equals(other.majorAlarm))
			return false;
		if (manufacturer == null) {
			if (other.manufacturer != null)
				return false;
		} else if (!manufacturer.equals(other.manufacturer))
			return false;
		if (minorAlarm == null) {
			if (other.minorAlarm != null)
				return false;
		} else if (!minorAlarm.equals(other.minorAlarm))
			return false;
		if (modelNumber == null) {
			if (other.modelNumber != null)
				return false;
		} else if (!modelNumber.equals(other.modelNumber))
			return false;
		if (pcsCurrentAC == null) {
			if (other.pcsCurrentAC != null)
				return false;
		} else if (!pcsCurrentAC.equals(other.pcsCurrentAC))
			return false;
		if (pcsStatus == null) {
			if (other.pcsStatus != null)
				return false;
		} else if (!pcsStatus.equals(other.pcsStatus))
			return false;
		if (pcsVoltageAC == null) {
			if (other.pcsVoltageAC != null)
				return false;
		} else if (!pcsVoltageAC.equals(other.pcsVoltageAC))
			return false;
		if (powerActiveAC == null) {
			if (other.powerActiveAC != null)
				return false;
		} else if (!powerActiveAC.equals(other.powerActiveAC))
			return false;
		if (powerApparentAC == null) {
			if (other.powerApparentAC != null)
				return false;
		} else if (!powerApparentAC.equals(other.powerApparentAC))
			return false;
		if (powerBattery == null) {
			if (other.powerBattery != null)
				return false;
		} else if (!powerBattery.equals(other.powerBattery))
			return false;
		if (powerOutputAC == null) {
			if (other.powerOutputAC != null)
				return false;
		} else if (!powerOutputAC.equals(other.powerOutputAC))
			return false;
		if (powerReactiveAC == null) {
			if (other.powerReactiveAC != null)
				return false;
		} else if (!powerReactiveAC.equals(other.powerReactiveAC))
			return false;
		if (pvInverterType != other.pvInverterType)
			return false;
		if (runningStatus != other.runningStatus)
			return false;
		if (serialNumber == null) {
			if (other.serialNumber != null)
				return false;
		} else if (!serialNumber.equals(other.serialNumber))
			return false;
		if (size == null) {
			if (other.size != null)
				return false;
		} else if (!size.equals(other.size))
			return false;
		if (tempBattery1 == null) {
			if (other.tempBattery1 != null)
				return false;
		} else if (!tempBattery1.equals(other.tempBattery1))
			return false;
		if (tempBattery2 == null) {
			if (other.tempBattery2 != null)
				return false;
		} else if (!tempBattery2.equals(other.tempBattery2))
			return false;
		if (tempBattery3 == null) {
			if (other.tempBattery3 != null)
				return false;
		} else if (!tempBattery3.equals(other.tempBattery3))
			return false;
		if (tempBatteryAvg == null) {
			if (other.tempBatteryAvg != null)
				return false;
		} else if (!tempBatteryAvg.equals(other.tempBatteryAvg))
			return false;
		if (tempBatteryMax == null) {
			if (other.tempBatteryMax != null)
				return false;
		} else if (!tempBatteryMax.equals(other.tempBatteryMax))
			return false;
		if (tempBatteryMin == null) {
			if (other.tempBatteryMin != null)
				return false;
		} else if (!tempBatteryMin.equals(other.tempBatteryMin))
			return false;
		if (watchdogTimer == null) {
			if (other.watchdogTimer != null)
				return false;
		} else if (!watchdogTimer.equals(other.watchdogTimer))
			return false;
		return true;
	}
	
	/** The Builder class to instantiate the PcsInverter model object.
	 * @author Gaurav Garg
	 *
	 */
	@JsonPOJOBuilder
	public static class PcsInverterBuilder{

		private Long id;
		private String deviceId;
		private String serialNumber;
		private String manufacturer;
		private String modelNumber;
		private String fwVersion;
		private PvInverterTypeEnum pvInverterType;
		private String size;
		
		private BigInteger powerOutputAC;
		private BigInteger pcsVoltageAC;
		private BigInteger pcsCurrentAC;
		private BigInteger powerActiveAC;
		private BigInteger powerReactiveAC;
		private BigInteger powerApparentAC;
		private BigInteger dcBusVoltage;
		private BigInteger frequencyGrid;
		private Long pcsStatus; // for UInt16 we are taking long because range would be more
		private RunningStatusEnum runningStatus;
		private BigInteger powerBattery;
		private Long watchdogTimer;
		private Long minorAlarm;
		private Long majorAlarm;
		private EmergencySwitchStatusEnum emergencySwitchStatus;
		private Long currentBattery;
		private BigInteger tempBattery1;
		private BigInteger tempBattery2;
		private BigInteger tempBattery3;
		private BigInteger tempBatteryMin;
		private BigInteger tempBatteryMax;
		private BigInteger tempBatteryAvg;
		private BigInteger battSoC;
		private BigInteger energyHourLast;
		private BigInteger energyDayLast;
		private BigInteger energyMonthLast;
		
		public PcsInverterBuilder(){};

		public PcsInverterBuilder(PcsInverter pcsInverter) {
			this.id = pcsInverter.getId();
			this.deviceId = pcsInverter.getDeviceId();
			this.serialNumber = pcsInverter.getSerialNumber();
			this.manufacturer = pcsInverter.getManufacturer();
			this.modelNumber = pcsInverter.getModelNumber();
			this.fwVersion = pcsInverter.getFwVersion();
			this.pvInverterType = pcsInverter.getPvInverterType();
			this.size = pcsInverter.getSize();
			this.powerOutputAC = pcsInverter.getPowerOutputAC();
			this.pcsVoltageAC = pcsInverter.getPcsVoltageAC();
			this.pcsCurrentAC = pcsInverter.getPcsCurrentAC();
			this.powerActiveAC = pcsInverter.getPowerActiveAC();
			this.powerReactiveAC = pcsInverter.getPowerReactiveAC();
			this.powerApparentAC = pcsInverter.getPowerApparentAC();
			this.dcBusVoltage = pcsInverter.getDcBusVoltage();
			this.frequencyGrid = pcsInverter.getFrequencyGrid();
			this.pcsStatus = pcsInverter.getPcsStatus();
			this.runningStatus = pcsInverter.getRunningStatus();
			this.powerBattery = pcsInverter.getPowerBattery();
			this.watchdogTimer = pcsInverter.getWatchdogTimer();
			this.minorAlarm = pcsInverter.getMinorAlarm();
			this.majorAlarm = pcsInverter.getMajorAlarm();
			this.emergencySwitchStatus = pcsInverter.emergencySwitchStatus;
			currentBattery = pcsInverter.getCurrentBattery();
			this.tempBattery1 = pcsInverter.getTempBattery1();
			this.tempBattery2 = pcsInverter.getTempBattery2();
			this.tempBattery3 = pcsInverter.getTempBattery3();
			this.tempBatteryMin = pcsInverter.getTempBatteryMin();
			this.tempBatteryMax = pcsInverter.getTempBatteryMax();
			this.tempBatteryAvg = pcsInverter.getTempBatteryAvg();
			this.battSoC = pcsInverter.getBattSoC();
			this.energyHourLast = pcsInverter.getEnergyHourLast();
			this.energyDayLast = pcsInverter.getEnergyDayLast();
			this.energyMonthLast = pcsInverter.getEnergyMonthLast();
		}

		/** Sets the id with builder class.
		 * @param id the id to set
		 */
		public PcsInverterBuilder withId(Long id) {
			this.id = id;
			return this;
		}
		
		/** Sets the deviceId with builder class.
		 * @param deviceId the deviceId to with
		 */
		public PcsInverterBuilder withDeviceId(String deviceId) {
			this.deviceId = deviceId;
			return this;
		}

		/**
		 * @param serialNumber the serialNumber to with
		 */
		public PcsInverterBuilder withSerialNumber(String serialNumber) {
			this.serialNumber = serialNumber;
			return this;
		}

		/**
		 * @param manufacturer the manufacturer to with
		 */
		public PcsInverterBuilder withManufacturer(String manufacturer) {
			this.manufacturer = manufacturer;
			return this;
		}

		/**
		 * @param modelNumber the modelNumber to with
		 */
		public PcsInverterBuilder withModelNumber(String modelNumber) {
			this.modelNumber = modelNumber;
			return this;
		}

		/**
		 * @param fwVersion the fwVersion to with
		 */
		public PcsInverterBuilder withFwVersion(String fwVersion) {
			this.fwVersion = fwVersion;
			return this;
		}

		/**
		 * @param pvInverterType the pvInverterType to with
		 */
		public PcsInverterBuilder withPvInverterType(PvInverterTypeEnum pvInverterType) {
			this.pvInverterType = pvInverterType;
			return this;
		}

		/**
		 * @param size the size to with
		 */
		public PcsInverterBuilder withSize(String size) {
			this.size = size;
			return this;
		}
		
		/**
		 * @param powerOutputAC the powerOutputAC to with
		 */
		public PcsInverterBuilder withPowerOutputAC(BigInteger powerOutputAC) {
			this.powerOutputAC = powerOutputAC;
			return this;
		}

		/**
		 * @param pcsVoltageAC the pcsVoltageAC to with
		 */
		public PcsInverterBuilder withPcsVoltageAC(BigInteger pcsVoltageAC) {
			this.pcsVoltageAC = pcsVoltageAC;
			return this;
		}

		/**
		 * @param pcsCurrentAC the pcsCurrentAC to with
		 */
		public PcsInverterBuilder withPcsCurrentAC(BigInteger pcsCurrentAC) {
			this.pcsCurrentAC = pcsCurrentAC;
			return this;
		}

		/**
		 * @param powerActiveAC the powerActiveAC to with
		 */
		public PcsInverterBuilder withPowerActiveAC(BigInteger powerActiveAC) {
			this.powerActiveAC = powerActiveAC;
			return this;
		}

		/**
		 * @param powerReactiveAC the powerReactiveAC to with
		 */
		public PcsInverterBuilder withPowerReactiveAC(BigInteger powerReactiveAC) {
			this.powerReactiveAC = powerReactiveAC;
			return this;
		}

		/**
		 * @param powerApparentAC the powerApparentAC to with
		 */
		public PcsInverterBuilder withPowerApparentAC(BigInteger powerApparentAC) {
			this.powerApparentAC = powerApparentAC;
			return this;
		}

		/**
		 * @param dcBusVoltage the dcBusVoltage to with
		 */
		public PcsInverterBuilder withDcBusVoltage(BigInteger dcBusVoltage) {
			this.dcBusVoltage = dcBusVoltage;
			return this;
		}

		/**
		 * @param frequencyGrid the frequencyGrid to with
		 */
		public PcsInverterBuilder withFrequencyGrid(BigInteger frequencyGrid) {
			this.frequencyGrid = frequencyGrid;
			return this;
		}

		/**
		 * @param pcsStatus the pcsStatus to with
		 */
		public PcsInverterBuilder withPcsStatus(Long pcsStatus) {
			this.pcsStatus = pcsStatus;
			return this;
		}

		/**
		 * @param runningStatus the runningStatus to with
		 */
		public PcsInverterBuilder withRunningStatus(RunningStatusEnum runningStatus) {
			this.runningStatus = runningStatus;
			return this;
		}

		/**
		 * @param powerBattery the powerBattery to with
		 */
		public PcsInverterBuilder withPowerBattery(BigInteger powerBattery) {
			this.powerBattery = powerBattery;
			return this;
		}

		/**
		 * @param watchdogTimer the watchdogTimer to with
		 */
		public PcsInverterBuilder withWatchdogTimer(Long watchdogTimer) {
			this.watchdogTimer = watchdogTimer;
			return this;
		}

		/**
		 * @param minorAlarm the minorAlarm to with
		 */
		public PcsInverterBuilder withMinorAlarm(Long minorAlarm) {
			this.minorAlarm = minorAlarm;
			return this;
		}

		/**
		 * @param majorAlarm the majorAlarm to with
		 */
		public PcsInverterBuilder withMajorAlarm(Long majorAlarm) {
			this.majorAlarm = majorAlarm;
			return this;
		}

		/**
		 * @param emergencySwitchStatus the emergencySwitchStatus to with
		 */
		public PcsInverterBuilder withEmergencySwitchStatus(EmergencySwitchStatusEnum emergencySwitchStatus) {
			this.emergencySwitchStatus = emergencySwitchStatus;
			return this;
		}

		/**
		 * @param currentBattery the currentBattery to with
		 */
		public PcsInverterBuilder withCurrentBattery(Long currentBattery) {
			this.currentBattery = currentBattery;
			return this;
		}

		/**
		 * @param tempBattery1 the tempBattery1 to with
		 */
		public PcsInverterBuilder withTempBattery1(BigInteger tempBattery1) {
			this.tempBattery1 = tempBattery1;
			return this;
		}

		/**
		 * @param tempBattery2 the tempBattery2 to with
		 */
		public PcsInverterBuilder withTempBattery2(BigInteger tempBattery2) {
			this.tempBattery2 = tempBattery2;
			return this;
		}

		/**
		 * @param tempBattery3 the tempBattery3 to with
		 */
		public PcsInverterBuilder withTempBattery3(BigInteger tempBattery3) {
			this.tempBattery3 = tempBattery3;
			return this;
		}

		/**
		 * @param tempBatteryMin the tempBatteryMin to with
		 */
		public PcsInverterBuilder withTempBatteryMin(BigInteger tempBatteryMin) {
			this.tempBatteryMin = tempBatteryMin;
			return this;
		}

		/**
		 * @param tempBatteryMax the tempBatteryMax to with
		 */
		public PcsInverterBuilder withTempBatteryMax(BigInteger tempBatteryMax) {
			this.tempBatteryMax = tempBatteryMax;
			return this;
		}

		/**
		 * @param tempBatteryAvg the tempBatteryAvg to with
		 */
		public PcsInverterBuilder withTempBatteryAvg(BigInteger tempBatteryAvg) {
			this.tempBatteryAvg = tempBatteryAvg;
			return this;
		}

		/**
		 * @param battSoC the battSoC to with
		 */
		public PcsInverterBuilder withBattSoC(BigInteger battSoC) {
			this.battSoC = battSoC;
			return this;
		}

		/**
		 * @param energyHourLast the energyHourLast to with
		 */
		public PcsInverterBuilder withEnergyHourLast(BigInteger energyHourLast) {
			this.energyHourLast = energyHourLast;
			return this;
		}

		/**
		 * @param energyDayLast the energyDayLast to with
		 */
		public PcsInverterBuilder withEnergyDayLast(BigInteger energyDayLast) {
			this.energyDayLast = energyDayLast;
			return this;
		}

		/**
		 * @param energyMonthLast the energyMonthLast to with
		 */
		public PcsInverterBuilder withEnergyMonthLast(BigInteger energyMonthLast) {
			this.energyMonthLast = energyMonthLast;
			return this;
		}
		
		
		/** The PcsInverter object instance set by builder class.
		 * @return the PcsInverter Instance
		 */
		public PcsInverter build() {
			return new PcsInverter(this);
		}
	}

	
}
