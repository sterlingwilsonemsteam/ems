/**
 * 
 */
package com.sw.ems.common.model.coredomain.impl.device;

import java.io.Serializable;

/**
 * @author hema
 *
 */
public class Device implements Serializable {
	
	/**
	 * The serial version UID.
	 */
	private static final long serialVersionUID = 1L;

	protected String deviceId;

	protected String serialNumber;

	protected String manufacturer;

	protected String modelNumber;

	protected String fwVersion;

	protected String pvInvType;

	protected String size;

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getModelNumber() {
		return modelNumber;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	public String getFwVersion() {
		return fwVersion;
	}

	public void setFwVersion(String fwVersion) {
		this.fwVersion = fwVersion;
	}

	public String getPvInvType() {
		return pvInvType;
	}

	public void setPvInvType(String pvInvType) {
		this.pvInvType = pvInvType;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}
	
	
}
