package com.sw.ems.common.model.coredomain.impl.topology;

import com.sw.ems.common.model.coredomain.api.topology.IPlant;
import com.sw.ems.common.model.coredomain.api.topology.IVirtualPowerPlant;
import com.sw.ems.common.model.coredomain.impl.topology.AbstractAsset;

import java.util.List;

/**
 *
 */
public class StandardVirtualPowerPlant extends AbstractAsset implements IVirtualPowerPlant {
    @Override
    public List<IPlant> getPlants() {
        return null;
    }
}
