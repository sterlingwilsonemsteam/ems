package com.sw.ems.common.model.coredomain.impl.device;

import com.sw.ems.common.model.coredomain.api.device.INonEnergyDevice;
import com.sw.ems.common.model.coredomain.impl.device.AbstractDevice;

/**
 *
 */
public abstract class AbstractNonEnergyDevice extends AbstractDevice implements INonEnergyDevice {
}
