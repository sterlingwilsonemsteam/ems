package com.sw.ems.common.model.coredomain.impl;

import java.io.Serializable;

import com.sw.ems.common.model.coredomain.api.device.PvInverterTypeEnum;


//TODO Move this
/**
 * @author gaurav
 *
 */
@SuppressWarnings("unused")
public class DeviceInformation implements Serializable {

	/**
	 * The serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	private String deviceId;
	private String serialNumber;
	private String manufacturer;
	private String modelNumber;
	private String fwVersion;
	private PvInverterTypeEnum pvInverterType;
	private String size;
	
	public DeviceInformation(DeviceInformationBuilder builder) {
		this.deviceId = builder.deviceId;
		this.serialNumber = builder.serialNumber;
		this.manufacturer = builder.manufacturer;
		this.modelNumber = builder.modelNumber;
		this.fwVersion = builder.fwVersion;
		this.pvInverterType = builder.pvInverterType;
		this.size = builder.size;
	}
	
	@Override
	public String toString() {
		return "DeviceInformation [deviceId=" + deviceId + ", serialNumber=" + serialNumber + ", manufacturer="
				+ manufacturer + ", modelNumber=" + modelNumber + ", fwVersion=" + fwVersion + ", pvInverterType="
				+ pvInverterType + ", size=" + size + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

	public static class DeviceInformationBuilder  {
		
		private String deviceId;
		private String serialNumber;
		private String manufacturer;
		private String modelNumber;
		private String fwVersion;
		private PvInverterTypeEnum pvInverterType;
		private String size;

		/**
		 * @param deviceId the deviceId to with
		 */
		public DeviceInformationBuilder withDeviceId(String deviceId) {
			this.deviceId = deviceId;
			return this;
		}

		/**
		 * @param serialNumber the serialNumber to with
		 */
		public DeviceInformationBuilder withSerialNumber(String serialNumber) {
			this.serialNumber = serialNumber;
			return this;
		}

		/**
		 * @param manufacturer the manufacturer to with
		 */
		public DeviceInformationBuilder withManufacturer(String manufacturer) {
			this.manufacturer = manufacturer;
			return this;
		}

		/**
		 * @param modelNumber the modelNumber to with
		 */
		public DeviceInformationBuilder withModelNumber(String modelNumber) {
			this.modelNumber = modelNumber;
			return this;
		}

		/**
		 * @param fwVersion the fwVersion to with
		 */
		public DeviceInformationBuilder withFwVersion(String fwVersion) {
			this.fwVersion = fwVersion;
			return this;
		}

		/**
		 * @param pvInverterType the pvInverterType to with
		 */
		public DeviceInformationBuilder withPvInverterType(PvInverterTypeEnum pvInverterType) {
			this.pvInverterType = pvInverterType;
			return this;
		}

		/**
		 * @param size the size to with
		 */
		public DeviceInformationBuilder withSize(String size) {
			this.size = size;
			return this;
		}
		
		/**
		 * @return the DeviceInformation 
		 */
		public DeviceInformation build() {
			return new DeviceInformation(this);
		}
	}

}
