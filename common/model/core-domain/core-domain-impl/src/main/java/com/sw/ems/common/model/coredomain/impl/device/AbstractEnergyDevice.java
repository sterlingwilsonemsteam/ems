package com.sw.ems.common.model.coredomain.impl.device;

import com.sw.ems.common.model.coredomain.api.device.IEnergyDevice;
import com.sw.ems.common.model.coredomain.impl.device.AbstractDevice;

/**
 *
 */
public abstract class AbstractEnergyDevice extends AbstractDevice implements IEnergyDevice {
}
