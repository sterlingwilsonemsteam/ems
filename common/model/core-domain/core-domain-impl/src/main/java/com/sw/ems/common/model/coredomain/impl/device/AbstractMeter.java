package com.sw.ems.common.model.coredomain.impl.device;

import com.sw.ems.common.model.coredomain.api.device.IMeter;

/**
 *
 */
public abstract class AbstractMeter extends AbstractNonEnergyDevice implements IMeter {
}
