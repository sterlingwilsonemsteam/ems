package com.sw.ems.common.model.coredomain.impl.device;

import com.sw.ems.common.model.coredomain.api.device.IDevice;
import com.sw.ems.common.model.coredomain.impl.topology.AbstractAsset;

/**
 *
 */
public abstract class AbstractDevice extends AbstractAsset implements IDevice {

    //@Override
    //TODO public IDataPayloadSet getDataPayloadSet() {
    //    return null;
    //}
}
