package com.sw.ems.common.model.coredomain.impl.device;

import com.sw.ems.common.model.coredomain.api.device.IDeviceMetadata;
import com.sw.ems.common.model.coredomain.api.device.inverter.IPVInverter;

/**
 *
 */
public class StandardPVInverter extends AbstractEnergyDevice implements IPVInverter {
    @Override
    public IDeviceMetadata getMetadata() {
        return null;
    }
}
