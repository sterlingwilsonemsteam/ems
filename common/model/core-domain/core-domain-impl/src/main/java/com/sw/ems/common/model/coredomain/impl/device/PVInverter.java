/**
 * 
 */
package com.sw.ems.common.model.coredomain.impl.device;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author hema
 *
 */
public class PVInverter extends Device {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;

	private long pvStatus;  
	
	private short runningStatus;

	private short watchdogTimer;

	private short minorAlarm;

	private short majorAlarm;
	
	private BigDecimal tempPVInv1;

	private BigDecimal tempPVInv2;

	private BigDecimal tempPVInv3;

	private BigDecimal powerOutputAC;

	private BigDecimal pvVoltageAC;

	private BigDecimal pvCurrentAC;

	private BigDecimal powerActiveAC;

	private BigDecimal powerReactiveAC;

	private BigDecimal powerApparentAC;

	private BigDecimal frequencyGrid;

	private BigDecimal energyMinuteLast;  

	private BigDecimal energyHourLast;

	private BigDecimal energyDayLast;

	private BigDecimal energyMonthLast;

	private BigDecimal energyYearLast;

	private BigDecimal pvVoltageString1;

	private long currentString1;

	private BigDecimal pvVoltageString2;

	private long currentString2;

	private BigDecimal pvVoltageString10;

	private long currentString10;	 

	private boolean onOffCommands;

	private short reset;

	private short operationMode;

	private short powerActivePercent;

	private short powerActive;

	private short powerReactive;

	private short battPowerChargeLimit;

	private short battPowerDischargeLimit;
	
	
	
	public PVInverter(String id, long pvStatus, short runningStatus) {
		super();
		this.id = id;
		this.pvStatus = pvStatus;
		this.runningStatus = runningStatus;
	}


	/**
	 * 
	 */
	public PVInverter() {
		super();
	}


	/**
	 * @param id
	 * @param pvStatus
	 * @param runningStatus
	 * @param watchdogTimer
	 * @param minorAlarm
	 * @param majorAlarm
	 * @param tempPVInv1
	 * @param tempPVInv2
	 * @param tempPVInv3
	 * @param powerOutputAC
	 * @param pvVoltageAC
	 * @param pvCurrentAC
	 * @param powerActiveAC
	 * @param powerReactiveAC
	 * @param powerApparentAC
	 * @param frequencyGrid
	 * @param energyMinuteLast
	 * @param energyHourLast
	 * @param energyDayLast
	 * @param energyMonthLast
	 * @param energyYearLast
	 * @param pvVoltageString1
	 * @param currentString1
	 * @param pvVoltageString2
	 * @param currentString2
	 * @param pvVoltageString10
	 * @param currentString10
	 * @param onOffCommands
	 * @param reset
	 * @param operationMode
	 * @param powerActivePercent
	 * @param powerActive
	 * @param powerReactive
	 * @param battPowerChargeLimit
	 * @param battPowerDischargeLimit
	 */
	@JsonCreator
	public PVInverter(
			@JsonProperty("id") String id, @JsonProperty("pvStatus")  long pvStatus, @JsonProperty("runningStatus")short runningStatus, @JsonProperty("watchdogTimer")short watchdogTimer, @JsonProperty("minorAlarm")  short minorAlarm, @JsonProperty("majorAlarm") 
			short majorAlarm, @JsonProperty("tempPVInv1")  BigDecimal tempPVInv1, @JsonProperty("tempPVInv2")  BigDecimal tempPVInv2, @JsonProperty("tempPVInv3")  BigDecimal tempPVInv3, @JsonProperty("powerOutputAC") 
			BigDecimal powerOutputAC, @JsonProperty("pvVoltageAC")  BigDecimal pvVoltageAC, @JsonProperty("pvCurrentAC")  BigDecimal pvCurrentAC, @JsonProperty("powerActiveAC")  BigDecimal powerActiveAC, @JsonProperty("powerReactiveAC") 
			BigDecimal powerReactiveAC, @JsonProperty("powerApparentAC")  BigDecimal powerApparentAC, @JsonProperty("frequencyGrid")  BigDecimal frequencyGrid, @JsonProperty("energyMinuteLast") 
			BigDecimal energyMinuteLast, @JsonProperty("energyHourLast")  BigDecimal energyHourLast, @JsonProperty("energyDayLast")  BigDecimal energyDayLast, @JsonProperty("energyMonthLast") 
			BigDecimal energyMonthLast, @JsonProperty("energyYearLast")  BigDecimal energyYearLast, @JsonProperty("pvVoltageString1")  BigDecimal pvVoltageString1, @JsonProperty("currentString1")  long currentString1, @JsonProperty("pvVoltageString2") 
			BigDecimal pvVoltageString2, @JsonProperty("currentString2")  long currentString2, @JsonProperty("pvVoltageString10")  BigDecimal pvVoltageString10, @JsonProperty("currentString10")  long currentString10, @JsonProperty("onOffCommands") 
			boolean onOffCommands, @JsonProperty("reset")  short reset, @JsonProperty("operationMode")  short operationMode, @JsonProperty("powerActivePercent")  short powerActivePercent, @JsonProperty("powerActive")  short powerActive, @JsonProperty("powerReactive") 
			short powerReactive, @JsonProperty("battPowerChargeLimit")  short battPowerChargeLimit, @JsonProperty("battPowerDischargeLimit")  short battPowerDischargeLimit) {
		super();
		this.id = id;
		this.pvStatus = pvStatus;
		this.runningStatus = runningStatus;
		this.watchdogTimer = watchdogTimer;
		this.minorAlarm = minorAlarm;
		this.majorAlarm = majorAlarm;
		this.tempPVInv1 = tempPVInv1;
		this.tempPVInv2 = tempPVInv2;
		this.tempPVInv3 = tempPVInv3;
		this.powerOutputAC = powerOutputAC;
		this.pvVoltageAC = pvVoltageAC;
		this.pvCurrentAC = pvCurrentAC;
		this.powerActiveAC = powerActiveAC;
		this.powerReactiveAC = powerReactiveAC;
		this.powerApparentAC = powerApparentAC;
		this.frequencyGrid = frequencyGrid;
		this.energyMinuteLast = energyMinuteLast;
		this.energyHourLast = energyHourLast;
		this.energyDayLast = energyDayLast;
		this.energyMonthLast = energyMonthLast;
		this.energyYearLast = energyYearLast;
		this.pvVoltageString1 = pvVoltageString1;
		this.currentString1 = currentString1;
		this.pvVoltageString2 = pvVoltageString2;
		this.currentString2 = currentString2;
		this.pvVoltageString10 = pvVoltageString10;
		this.currentString10 = currentString10;
		this.onOffCommands = onOffCommands;
		this.reset = reset;
		this.operationMode = operationMode;
		this.powerActivePercent = powerActivePercent;
		this.powerActive = powerActive;
		this.powerReactive = powerReactive;
		this.battPowerChargeLimit = battPowerChargeLimit;
		this.battPowerDischargeLimit = battPowerDischargeLimit;
	}


	/**
	 * @return the pvStatus
	 */
	public long getPvStatus() {
		return pvStatus;
	}

	/**
	 * @param pvStatus the pvStatus to set
	 */
	public void setPvStatus(long pvStatus) {
		this.pvStatus = pvStatus;
	}

	/**
	 * @return the runningStatus
	 */
	public short getRunningStatus() {
		return runningStatus;
	}

	/**
	 * @param runningStatus the runningStatus to set
	 */
	public void setRunningStatus(short runningStatus) {
		this.runningStatus = runningStatus;
	}

	/**
	 * @return the watchdogTimer
	 */
	public short getWatchdogTimer() {
		return watchdogTimer;
	}

	/**
	 * @param watchdogTimer the watchdogTimer to set
	 */
	public void setWatchdogTimer(short watchdogTimer) {
		this.watchdogTimer = watchdogTimer;
	}

	/**
	 * @return the minorAlarm
	 */
	public short getMinorAlarm() {
		return minorAlarm;
	}

	/**
	 * @param minorAlarm the minorAlarm to set
	 */
	public void setMinorAlarm(short minorAlarm) {
		this.minorAlarm = minorAlarm;
	}

	/**
	 * @return the majorAlarm
	 */
	public short getMajorAlarm() {
		return majorAlarm;
	}

	/**
	 * @param majorAlarm the majorAlarm to set
	 */
	public void setMajorAlarm(short majorAlarm) {
		this.majorAlarm = majorAlarm;
	}

	/**
	 * @return the tempPVInv1
	 */
	public BigDecimal getTempPVInv1() {
		return tempPVInv1;
	}

	/**
	 * @param tempPVInv1 the tempPVInv1 to set
	 */
	public void setTempPVInv1(BigDecimal tempPVInv1) {
		this.tempPVInv1 = tempPVInv1;
	}

	/**
	 * @return the tempPVInv2
	 */
	public BigDecimal getTempPVInv2() {
		return tempPVInv2;
	}

	/**
	 * @param tempPVInv2 the tempPVInv2 to set
	 */
	public void setTempPVInv2(BigDecimal tempPVInv2) {
		this.tempPVInv2 = tempPVInv2;
	}

	/**
	 * @return the tempPVInv3
	 */
	public BigDecimal getTempPVInv3() {
		return tempPVInv3;
	}

	/**
	 * @param tempPVInv3 the tempPVInv3 to set
	 */
	public void setTempPVInv3(BigDecimal tempPVInv3) {
		this.tempPVInv3 = tempPVInv3;
	}

	/**
	 * @return the powerOutputAC
	 */
	public BigDecimal getPowerOutputAC() {
		return powerOutputAC;
	}

	/**
	 * @param powerOutputAC the powerOutputAC to set
	 */
	public void setPowerOutputAC(BigDecimal powerOutputAC) {
		this.powerOutputAC = powerOutputAC;
	}

	/**
	 * @return the pvVoltageAC
	 */
	public BigDecimal getPvVoltageAC() {
		return pvVoltageAC;
	}

	/**
	 * @param pvVoltageAC the pvVoltageAC to set
	 */
	public void setPvVoltageAC(BigDecimal pvVoltageAC) {
		this.pvVoltageAC = pvVoltageAC;
	}

	/**
	 * @return the pvCurrentAC
	 */
	public BigDecimal getPvCurrentAC() {
		return pvCurrentAC;
	}

	/**
	 * @param pvCurrentAC the pvCurrentAC to set
	 */
	public void setPvCurrentAC(BigDecimal pvCurrentAC) {
		this.pvCurrentAC = pvCurrentAC;
	}

	/**
	 * @return the powerActiveAC
	 */
	public BigDecimal getPowerActiveAC() {
		return powerActiveAC;
	}

	/**
	 * @param powerActiveAC the powerActiveAC to set
	 */
	public void setPowerActiveAC(BigDecimal powerActiveAC) {
		this.powerActiveAC = powerActiveAC;
	}

	/**
	 * @return the powerReactiveAC
	 */
	public BigDecimal getPowerReactiveAC() {
		return powerReactiveAC;
	}

	/**
	 * @param powerReactiveAC the powerReactiveAC to set
	 */
	public void setPowerReactiveAC(BigDecimal powerReactiveAC) {
		this.powerReactiveAC = powerReactiveAC;
	}

	/**
	 * @return the powerApparentAC
	 */
	public BigDecimal getPowerApparentAC() {
		return powerApparentAC;
	}

	/**
	 * @param powerApparentAC the powerApparentAC to set
	 */
	public void setPowerApparentAC(BigDecimal powerApparentAC) {
		this.powerApparentAC = powerApparentAC;
	}

	/**
	 * @return the frequencyGrid
	 */
	public BigDecimal getFrequencyGrid() {
		return frequencyGrid;
	}

	/**
	 * @param frequencyGrid the frequencyGrid to set
	 */
	public void setFrequencyGrid(BigDecimal frequencyGrid) {
		this.frequencyGrid = frequencyGrid;
	}

	/**
	 * @return the energyMinuteLast
	 */
	public BigDecimal getEnergyMinuteLast() {
		return energyMinuteLast;
	}

	/**
	 * @param energyMinuteLast the energyMinuteLast to set
	 */
	public void setEnergyMinuteLast(BigDecimal energyMinuteLast) {
		this.energyMinuteLast = energyMinuteLast;
	}

	/**
	 * @return the energyHourLast
	 */
	public BigDecimal getEnergyHourLast() {
		return energyHourLast;
	}

	/**
	 * @param energyHourLast the energyHourLast to set
	 */
	public void setEnergyHourLast(BigDecimal energyHourLast) {
		this.energyHourLast = energyHourLast;
	}

	/**
	 * @return the energyDayLast
	 */
	public BigDecimal getEnergyDayLast() {
		return energyDayLast;
	}

	/**
	 * @param energyDayLast the energyDayLast to set
	 */
	public void setEnergyDayLast(BigDecimal energyDayLast) {
		this.energyDayLast = energyDayLast;
	}

	/**
	 * @return the energyMonthLast
	 */
	public BigDecimal getEnergyMonthLast() {
		return energyMonthLast;
	}

	/**
	 * @param energyMonthLast the energyMonthLast to set
	 */
	public void setEnergyMonthLast(BigDecimal energyMonthLast) {
		this.energyMonthLast = energyMonthLast;
	}

	/**
	 * @return the energyYearLast
	 */
	public BigDecimal getEnergyYearLast() {
		return energyYearLast;
	}

	/**
	 * @param energyYearLast the energyYearLast to set
	 */
	public void setEnergyYearLast(BigDecimal energyYearLast) {
		this.energyYearLast = energyYearLast;
	}

	/**
	 * @return the pvVoltageString1
	 */
	public BigDecimal getPvVoltageString1() {
		return pvVoltageString1;
	}

	/**
	 * @param pvVoltageString1 the pvVoltageString1 to set
	 */
	public void setPvVoltageString1(BigDecimal pvVoltageString1) {
		this.pvVoltageString1 = pvVoltageString1;
	}

	/**
	 * @return the currentString1
	 */
	public long getCurrentString1() {
		return currentString1;
	}

	/**
	 * @param currentString1 the currentString1 to set
	 */
	public void setCurrentString1(long currentString1) {
		this.currentString1 = currentString1;
	}

	/**
	 * @return the pvVoltageString2
	 */
	public BigDecimal getPvVoltageString2() {
		return pvVoltageString2;
	}

	/**
	 * @param pvVoltageString2 the pvVoltageString2 to set
	 */
	public void setPvVoltageString2(BigDecimal pvVoltageString2) {
		this.pvVoltageString2 = pvVoltageString2;
	}

	/**
	 * @return the currentString2
	 */
	public long getCurrentString2() {
		return currentString2;
	}

	/**
	 * @param currentString2 the currentString2 to set
	 */
	public void setCurrentString2(long currentString2) {
		this.currentString2 = currentString2;
	}

	/**
	 * @return the pvVoltageString10
	 */
	public BigDecimal getPvVoltageString10() {
		return pvVoltageString10;
	}

	/**
	 * @param pvVoltageString10 the pvVoltageString10 to set
	 */
	public void setPvVoltageString10(BigDecimal pvVoltageString10) {
		this.pvVoltageString10 = pvVoltageString10;
	}

	/**
	 * @return the currentString10
	 */
	public long getCurrentString10() {
		return currentString10;
	}

	/**
	 * @param currentString10 the currentString10 to set
	 */
	public void setCurrentString10(long currentString10) {
		this.currentString10 = currentString10;
	}

	/**
	 * @return the onOffCommands
	 */
	public boolean isOnOffCommands() {
		return onOffCommands;
	}

	/**
	 * @param onOffCommands the onOffCommands to set
	 */
	public void setOnOffCommands(boolean onOffCommands) {
		this.onOffCommands = onOffCommands;
	}

	/**
	 * @return the reset
	 */
	public short getReset() {
		return reset;
	}

	/**
	 * @param reset the reset to set
	 */
	public void setReset(short reset) {
		this.reset = reset;
	}

	/**
	 * @return the operationMode
	 */
	public short getOperationMode() {
		return operationMode;
	}

	/**
	 * @param operationMode the operationMode to set
	 */
	public void setOperationMode(short operationMode) {
		this.operationMode = operationMode;
	}

	/**
	 * @return the powerActivePercent
	 */
	public short getPowerActivePercent() {
		return powerActivePercent;
	}

	/**
	 * @param powerActivePercent the powerActivePercent to set
	 */
	public void setPowerActivePercent(short powerActivePercent) {
		this.powerActivePercent = powerActivePercent;
	}

	/**
	 * @return the powerActive
	 */
	public short getPowerActive() {
		return powerActive;
	}

	/**
	 * @param powerActive the powerActive to set
	 */
	public void setPowerActive(short powerActive) {
		this.powerActive = powerActive;
	}

	/**
	 * @return the powerReactive
	 */
	public short getPowerReactive() {
		return powerReactive;
	}

	/**
	 * @param powerReactive the powerReactive to set
	 */
	public void setPowerReactive(short powerReactive) {
		this.powerReactive = powerReactive;
	}

	/**
	 * @return the battPowerChargeLimit
	 */
	public short getBattPowerChargeLimit() {
		return battPowerChargeLimit;
	}

	/**
	 * @param battPowerChargeLimit the battPowerChargeLimit to set
	 */
	public void setBattPowerChargeLimit(short battPowerChargeLimit) {
		this.battPowerChargeLimit = battPowerChargeLimit;
	}

	/**
	 * @return the battPowerDischargeLimit
	 */
	public short getBattPowerDischargeLimit() {
		return battPowerDischargeLimit;
	}

	/**
	 * @param battPowerDischargeLimit the battPowerDischargeLimit to set
	 */
	public void setBattPowerDischargeLimit(short battPowerDischargeLimit) {
		this.battPowerDischargeLimit = battPowerDischargeLimit;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}



	@Override
	public String toString() {
		return "PVInverter [id=" + id + ", pvStatus=" + pvStatus + ", runningStatus=" + runningStatus
				+ ", watchdogTimer=" + watchdogTimer + ", minorAlarm=" + minorAlarm + ", majorAlarm=" + majorAlarm
				+ ", tempPVInv1=" + tempPVInv1 + ", tempPVInv2=" + tempPVInv2 + ", tempPVInv3=" + tempPVInv3
				+ ", powerOutputAC=" + powerOutputAC + ", pvVoltageAC=" + pvVoltageAC + ", pvCurrentAC=" + pvCurrentAC
				+ ", powerActiveAC=" + powerActiveAC + ", powerReactiveAC=" + powerReactiveAC + ", powerApparentAC="
				+ powerApparentAC + ", frequencyGrid=" + frequencyGrid + ", energyMinuteLast=" + energyMinuteLast
				+ ", energyHourLast=" + energyHourLast + ", energyDayLast=" + energyDayLast + ", energyMonthLast="
				+ energyMonthLast + ", energyYearLast=" + energyYearLast + ", pvVoltageString1=" + pvVoltageString1
				+ ", currentString1=" + currentString1 + ", pvVoltageString2=" + pvVoltageString2 + ", currentString2="
				+ currentString2 + ", pvVoltageString10=" + pvVoltageString10 + ", currentString10=" + currentString10
				+ ", onOffCommands=" + onOffCommands + ", reset=" + reset + ", operationMode=" + operationMode
				+ ", powerActivePercent=" + powerActivePercent + ", powerActive=" + powerActive + ", powerReactive="
				+ powerReactive + ", battPowerChargeLimit=" + battPowerChargeLimit + ", battPowerDischargeLimit="
				+ battPowerDischargeLimit + "]";
	}
	
}