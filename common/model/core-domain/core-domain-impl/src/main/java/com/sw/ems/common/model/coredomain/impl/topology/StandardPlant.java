package com.sw.ems.common.model.coredomain.impl.topology;

import com.sw.ems.common.model.coredomain.api.device.IDevice;
import com.sw.ems.common.model.coredomain.api.topology.IDeviceGateway;
import com.sw.ems.common.model.coredomain.api.topology.IPlant;
import com.sw.ems.common.model.coredomain.impl.topology.AbstractAsset;

import java.util.List;

/**
 *
 */
public final class StandardPlant extends AbstractAsset implements IPlant {

    @Override
    public List<IDevice> getDevices() {
        return null;
    }

    @Override
    public List<IDeviceGateway> getDeviceGateways() {
        return null;
    }
}
