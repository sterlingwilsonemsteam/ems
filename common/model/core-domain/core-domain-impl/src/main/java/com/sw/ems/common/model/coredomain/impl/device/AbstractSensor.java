package com.sw.ems.common.model.coredomain.impl.device;

import com.sw.ems.common.model.coredomain.api.device.ISensor;

/**
 *
 */
public abstract class AbstractSensor extends AbstractNonEnergyDevice implements ISensor {
}
