package com.sw.ems.common.model.coredomain.api.topology;

import com.sw.ems.common.model.coredomain.api.IDomainObject;

/**
 *  An asset is an entity in a topology configuration.  It can be identified and
 *  monitored.  An asset can be related to other assets in a parent-child relationship.
 */
public interface IAsset extends IDomainObject {
    //TODO Owned or just assets? - This relationship needs to be clarified
    IAsset getOwnedAssets();
}
