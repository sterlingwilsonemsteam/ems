package com.sw.ems.common.model.coredomain.api.device.inverter;

public enum InverterModeType {
    START,
    STOP,
    SLEEP;
}
