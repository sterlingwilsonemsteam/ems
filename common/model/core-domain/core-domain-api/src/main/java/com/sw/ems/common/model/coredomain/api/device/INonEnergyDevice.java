package com.sw.ems.common.model.coredomain.api.device;

import com.sw.ems.common.model.coredomain.api.device.IDevice;

/**
 *
 */
//TODO Such things such as weather monitor sensors, IoT vibration sensors, Wind/Solar sensors
public interface INonEnergyDevice extends IDevice {
}
