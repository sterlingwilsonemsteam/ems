package com.sw.ems.common.model.coredomain.api.device.attribute;

public interface IAttribute<T> {
    AttributeType getType();
    T getValue();
}
