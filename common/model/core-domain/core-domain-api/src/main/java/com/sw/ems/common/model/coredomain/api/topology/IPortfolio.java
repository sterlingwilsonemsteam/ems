package com.sw.ems.common.model.coredomain.api.topology;

import com.sw.ems.common.model.coredomain.api.IDomainObject;
import com.sw.ems.common.model.coredomain.api.topology.IAsset;

import java.util.List;

/**
 *
 */
public interface IPortfolio extends IDomainObject {
    List<IAsset> getAssets();
}
