package com.sw.ems.common.model.coredomain.api.device.configuration;

/**
 *
 */
public interface IPVInverterConfiguration extends IDeviceConfiguration {

    //TODO Use enums or inheritence
    /**
     *
     * @return
     */
    String getInverterType();

    //TODO What type of units?  Talk to TW.
    /**
     * Get the inverter size.
     *
     * @return
     */
    String getSize();
}
