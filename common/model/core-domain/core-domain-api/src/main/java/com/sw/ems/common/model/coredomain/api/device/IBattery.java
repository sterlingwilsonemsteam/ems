package com.sw.ems.common.model.coredomain.api.device;

/**
 *  Logical data representation of a physical battery.
 */
public interface IBattery extends IEnergyDevice {
    //TODO Define data points and methods
}
