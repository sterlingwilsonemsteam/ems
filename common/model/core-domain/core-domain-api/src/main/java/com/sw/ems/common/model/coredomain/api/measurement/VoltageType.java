package com.sw.ems.common.model.coredomain.api.measurement;

public enum VoltageType {
    AC,
    DC;
}
