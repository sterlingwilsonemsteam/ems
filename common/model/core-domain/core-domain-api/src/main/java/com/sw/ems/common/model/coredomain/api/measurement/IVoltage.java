package com.sw.ems.common.model.coredomain.api.measurement;

import java.math.BigDecimal;

public interface IVoltage {
    VoltageType getType();
    VoltageUnitType getUnit();
    BigDecimal getValue();
}
