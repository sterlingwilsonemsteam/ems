package com.sw.ems.common.model.coredomain.api.topology;

import com.sw.ems.common.model.coredomain.api.topology.IAsset;
import com.sw.ems.common.model.coredomain.api.topology.IPlant;

import java.util.List;

//TODO Also known as a "Control Group"
//TODO A Virtual Power Plant with just one Plant is just a Plant
/**
 *
 */
public interface IVirtualPowerPlant extends IAsset {
    List<IPlant> getPlants();
}
