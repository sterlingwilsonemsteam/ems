package com.sw.ems.common.model.coredomain.api.measurement;

import com.sw.ems.common.model.coredomain.api.IDomainObject;

import java.math.BigDecimal;

/**
 *
 */
public interface IPower extends IDomainObject {
    BigDecimal getValue();
    PowerUnitType getUnit();
}
