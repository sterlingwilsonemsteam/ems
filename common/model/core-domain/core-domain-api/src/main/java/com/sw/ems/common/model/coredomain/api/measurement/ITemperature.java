package com.sw.ems.common.model.coredomain.api.measurement;

import java.math.BigDecimal;

public interface ITemperature {
    TemperatureUnitType getUnit();
    BigDecimal getValue();
}
