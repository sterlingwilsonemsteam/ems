package com.sw.ems.common.model.coredomain.api.device;

import com.sw.ems.common.model.coredomain.api.device.IDevice;

/**
 * Photo voltaic device
 */
public interface IPV extends IDevice {
}
