package com.sw.ems.common.model.coredomain.api.device.configuration;

/**
 *
 */
public interface IDeviceConfiguration {

    /**
     *
     * @return
     */
    String getDeviceId();

    /**
     *
     * @return
     */
    String getModelName();

    /**
     *
     * @return
     */
    String getSerialNumber();

    /**
     *
     * @return
     */
    String getManufacturer();

    /**
     *
     * @return
     */
    String getFirmwareVersion();
}
