package com.sw.ems.common.model.coredomain.api.device;

import com.sw.ems.common.model.coredomain.api.topology.IAsset;

/**
 * Logical data representation of a physical device.
 */
public interface IDevice extends IAsset {
    //TODO IDataPayloadSet getDataPayloadSet();
    IDeviceMetadata getMetadata();
}
