package com.sw.ems.common.model.coredomain.api.device;

/** The Emergency Switch Status Enum.
 * 
 * @author Gaurav Garg
 * @version 1.0
 * @since 1.0
 * 
 */
public enum EmergencySwitchStatusEnum {
	ON(0), OFF(1);

	private final Integer value;

	/** The Constructor.
	 * @param the value
	 */
	private EmergencySwitchStatusEnum(Integer value) {
		this.value = value;
	}

	/** Gets the value.
	 * @return the value
	 */
	public Integer getValue() {
		return value;
	}
}
