package com.sw.ems.common.model.coredomain.api.device;

import java.util.HashMap;
import java.util.Map;

/** The Running Status Enum.
 * 
 * @author gaurav
 * @version 1.0
 * @since 1.0
 */
public enum RunningStatusEnum {

	START(1, "Start"), 
	STOP(2, "Stop");

	private final Integer value;
	private final String text;

	/**
	 * A mapping between the integer code and its corresponding text to facilitate
	 * lookup by code.
	 */
	private static Map<Integer, RunningStatusEnum> valueToTextMapping;

	/** The RunningStatusEnum Constructor.
	 * @param value
	 * @param text
	 */
	private RunningStatusEnum(Integer value, String text) {
		this.value = value;
		this.text = text;
	}

	/**
	 * Utility method to get the text based on its corresponding integer code.
	 * 
	 * @param integer code.
	 * @return text.
	 */
	public static RunningStatusEnum getRunningStatus(Integer i) {
		if (valueToTextMapping == null) {
			initMapping();
		}
		return valueToTextMapping.get(i);
	}

	/** This method is used for mapping between text and corresponding integer code.
	 * 
	 */
	private static void initMapping() {
		valueToTextMapping = new HashMap<>();
		for (RunningStatusEnum s : values()) {
			valueToTextMapping.put(s.value, s);
		}
	}

	/** Gets the value.
	 * @return the value.
	 */
	public Integer getValue() {
		return value;
	}

	/** Gets the text.
	 * @return the text.
	 */
	public String getText() {
		return text;
	}

	/** The toString method.
	 *
	 */
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("RunningStatus");
		sb.append("{value=").append(value);
		sb.append(", text='").append(text).append('\'');
		sb.append('}');
		return sb.toString();
	}

}
