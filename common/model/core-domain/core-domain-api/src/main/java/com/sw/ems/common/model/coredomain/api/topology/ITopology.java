package com.sw.ems.common.model.coredomain.api.topology;

import com.sw.ems.common.model.coredomain.api.IDomainObject;

import java.util.List;

/**
 * Representation of a network of inter communicating assets.
 */
public interface ITopology extends IDomainObject {
    List<ITopologyEntity> getEntities();
}
