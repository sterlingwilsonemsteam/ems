package com.sw.ems.common.model.coredomain.api.device.inverter;

/**
 *
 */
public interface IPVInverter {

    // (device level information)

    //TODO UInt16 PVStatus        (range: 0 to 100)

    //TODO here we need to define allowable states…

    //TODO UInt16 RunningStatus (range: 0 to 10)

    InverterModeType getMode();

    UInt16 WatchdogTimer   (range 0 to 65535)

    Uint16 Minor Alarm (range: 0 to 2) :question_mark:

    Uint16 Major Alarm (range: 0 to 2)

    Float TempPVInv1       (range: -100 to +1000)

    Float TempPVInv2       (range: -100 to +1000)

    Float TempPVInv3      (range: -100 to +1000)



(AC side information)

    Float PowerOutputAC      (range: 0 to PVPowerMax)

    Float PVVoltageAC         (range: 0 to PVVoltageMax)

    Float PVCurrentAC         (range: 0 to PVCurrentMax)

    Float PowerActiveAC        (range: 0 to PVPowerActiveMax)

    Float PowerReactiveAC    (range: 0 to PVPowerReactiveMax)   [kVAr]

    Float PowerApparentAC  (range: 0 to PVPowerApparentMax) [kVa]

    Float FrequencyGrid   (range: 0 to 100)    [Hz]

//specify AC or DC side?

    Float EnergyMinuteLast    (range:  0 to TBD)

    Float EnergyHourLast    (range:  0 to TBD)

    Float EnergyDayLast    (range:  0 to TBD)

    Float EnergyMonthLast    (range:  0 to TBD)

    Float EnergyYearLast    (range:  0 to TBD)



( String information )

    Float PVVoltageString1       (Range:  0 to VoltageStringMax)

    UInt32 CurrentString1         (range: 0 to StringCurrentMax)

    Float PVVoltageString2       (Range:  0 to VoltageStringMax)

    UInt32 CurrentString2         (range: 0 to StringCurrentMax)

… (could be up to 10 or more strings)

    Float PVVoltageString10       (Range:  0 to VoltageStringMax)

    UInt32 CurrentString10         (range: 0 to StringCurrentMax)
}
