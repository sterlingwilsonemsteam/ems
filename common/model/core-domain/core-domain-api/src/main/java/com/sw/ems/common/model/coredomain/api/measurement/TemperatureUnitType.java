package com.sw.ems.common.model.coredomain.api.measurement;

public enum TemperatureUnitType {
    CELSIUS,
    FAHRENHEIT;
}
