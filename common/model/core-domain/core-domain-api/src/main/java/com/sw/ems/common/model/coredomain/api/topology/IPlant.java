package com.sw.ems.common.model.coredomain.api.topology;

import com.sw.ems.common.model.coredomain.api.device.IDevice;

import java.util.List;

/**
 *
 */
public interface IPlant extends IAsset, ITopologyEntity {
    List<IDevice> getDevices();
    List<IDeviceGateway> getDeviceGateways();
}
