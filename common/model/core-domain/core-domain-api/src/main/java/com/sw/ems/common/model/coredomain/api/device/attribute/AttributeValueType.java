package com.sw.ems.common.model.coredomain.api.device.attribute;

public enum AttributeValueType {
    STRING,
    BOOLEAN,
    NUMERIC;
}
