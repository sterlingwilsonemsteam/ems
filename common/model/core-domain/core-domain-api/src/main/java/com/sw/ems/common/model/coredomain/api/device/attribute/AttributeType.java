package com.sw.ems.common.model.coredomain.api.device.attribute;

public enum AttributeType {
    READ_ONLY,
    READ_WRITE;
}
