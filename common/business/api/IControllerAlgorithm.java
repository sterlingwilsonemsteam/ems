package sw.business.api;

/**
 * A controller algorithm that operates on asset data.
 */
public interface IControllerAlgorithm extends IAlgorithm {
}
