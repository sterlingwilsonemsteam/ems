package com.sw.ems.common.service;

import java.time.Instant;

public interface IHealthCheckResult {
    Instant getInstant();
}
