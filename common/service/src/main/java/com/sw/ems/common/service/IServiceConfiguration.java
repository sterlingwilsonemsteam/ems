package com.sw.ems.common.service;

public interface IServiceConfiguration {
    boolean healthCheckEnabled();
}
