package com.sw.ems.common.service;

public interface IService {
    IHealthCheckResult healthCheck();
}
