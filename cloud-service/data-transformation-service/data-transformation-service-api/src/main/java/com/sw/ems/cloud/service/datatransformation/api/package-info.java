/**
 * Ability to be deployed in multiple places, gateway or cloud.
 *
 * TODO:
 *    Configure transformations for a specific instance.
 */
package com.sw.ems.cloud.service.datatransformation.api;