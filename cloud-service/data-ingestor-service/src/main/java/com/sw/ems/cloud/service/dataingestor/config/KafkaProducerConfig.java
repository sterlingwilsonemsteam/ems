/**
 * 
 */
package com.sw.ems.cloud.service.dataingestor.config;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import com.sw.ems.common.KafkaConstants;
import com.sw.ems.common.model.coredomain.impl.device.Device;

/**
 * @author hema
 *
 */
@Configuration
public class KafkaProducerConfig {


    @Bean
    public ProducerFactory<String, Device> producerFactory() {
        return new DefaultKafkaProducerFactory<>(producerConfigs(), stringKeySerializer(), workUnitJsonSerializer());
    }

    @Bean
    public Map<String, Object> producerConfigs() {
        Map<String, Object> props = new HashMap<>();
      //  props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProducerProperties.getBootstrap());
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaConstants.KAFKA_SERVER+":"+KafkaConstants.KAFKA_PORT);
      
        return props;
    }

    @Bean
    public KafkaTemplate<String, Device> workUnitsKafkaTemplate() {
        KafkaTemplate<String, Device> kafkaTemplate =  new KafkaTemplate<>(producerFactory());
        //kafkaTemplate.setDefaultTopic(kafkaProducerProperties.getTopic());
        kafkaTemplate.setDefaultTopic(KafkaConstants.KAFKA_TOPIC);
        return kafkaTemplate;
    }

    @Bean
    public Serializer stringKeySerializer() {
        return new StringSerializer();
    }

    @Bean
    public Serializer workUnitJsonSerializer() {
        return new JsonSerializer();
    }
}


