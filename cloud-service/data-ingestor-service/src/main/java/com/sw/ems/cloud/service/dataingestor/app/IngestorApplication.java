package com.sw.ems.cloud.service.dataingestor.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author hema
 *
 */
@SpringBootApplication
@ComponentScan("com.sw.ems.cloud.service.*")
//@SpringBootApplication(scanBasePackages={"com.sw.ems.cloud.service.*"})

public class IngestorApplication{

	public static void main(String[] args) {

		SpringApplication.run(IngestorApplication.class, args);

	}
	
	/*
	 * @Bean public PVInverterSendData schedulerRunner() { return new
	 * PVInverterSendData(); }
	 */
}
