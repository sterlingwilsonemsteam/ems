package com.sw.ems.cloud.service.dataingestor.impl;

import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;

import com.sw.ems.cloud.service.dataingestor.api.IDataIngestorService;

/**
 *
 */
public class StandardDataIngestorService<T>  implements IDataIngestorService<T>  {

	@Autowired
	private KafkaTemplate<String, T> template;
	 
	  private static final Logger LOGGER = LoggerFactory.getLogger(Service.class);
	  
	  public T dispatch(T t) { 
		 try {
	        	
			 
			 	LOGGER.info("::::::::::::::::::IN BaseServiceImpl::::::::::::" );
	            SendResult<String, T> sendResult = template.sendDefault(t).get();
	           
	            RecordMetadata recordMetadata = sendResult.getRecordMetadata();
	            LOGGER.info("topic = {}, partition = {}, offset = {}, workUnit = {}",
	                    recordMetadata.topic(), recordMetadata.partition(), recordMetadata.offset(), t);
	           
	            return t;
	            
	        } catch (Exception e) {
	            throw new RuntimeException(e);
	        }
		
		}
}
