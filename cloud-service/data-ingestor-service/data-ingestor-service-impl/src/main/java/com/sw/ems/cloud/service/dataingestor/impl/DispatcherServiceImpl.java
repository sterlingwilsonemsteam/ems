/**
 * 
 */
package com.sw.ems.cloud.service.dataingestor.impl;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.sw.ems.cloud.service.dataingestor.api.DispatcherService;
import com.sw.ems.common.model.coredomain.impl.device.Device;

/**
 * @author hema
 *
 */
@Service
@Repository
public class DispatcherServiceImpl extends StandardDataIngestorService<Device> implements DispatcherService{

}
