package com.sw.ems.cloud.service.dataingestor.controller;

import java.math.BigInteger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.sw.ems.cloud.service.dataingestor.impl.PCSInverterServiceImpl;
import com.sw.ems.common.model.coredomain.api.device.EmergencySwitchStatusEnum;
import com.sw.ems.common.model.coredomain.api.device.PvInverterTypeEnum;
import com.sw.ems.common.model.coredomain.api.device.RunningStatusEnum;
import com.sw.ems.common.model.coredomain.impl.device.PcsInverter;
import com.sw.ems.common.model.coredomain.impl.device.PcsInverter.PcsInverterBuilder;
@Component
public class PVInverterSendData implements CommandLineRunner{

	
	@Autowired private PCSInverterServiceImpl pcsInverterServiceImpl;

	public static void main(String[] args) {
        // Recon Logic
		
    }
	@Override
	public void run(String... args) throws Exception {
		
    	
    	PcsInverterBuilder builder = new PcsInverterBuilder();
    	builder.withId(10l);
		builder.withDeviceId("12");
		builder.withSerialNumber("12");
		builder.withManufacturer("Sterling&Wilson");
		builder.withModelNumber("M12");
		builder.withFwVersion("V12");
		builder.withPvInverterType(PvInverterTypeEnum.CENTRAL);
		builder.withSize("S12");
		builder.withPowerOutputAC(BigInteger.valueOf(23));
		builder.withPcsVoltageAC(BigInteger.valueOf(12));
		builder.withPcsCurrentAC(BigInteger.valueOf(12));
		builder.withPowerActiveAC(BigInteger.valueOf(12));
		builder.withPowerReactiveAC(BigInteger.valueOf(12));
		builder.withPowerApparentAC(BigInteger.valueOf(134));
		builder.withDcBusVoltage(BigInteger.valueOf(23));
		builder.withFrequencyGrid(BigInteger.valueOf(12));
		builder.withPcsStatus(34l);
		builder.withRunningStatus(RunningStatusEnum.START);
		builder.withPowerBattery(BigInteger.valueOf(2));
		builder.withWatchdogTimer(5l);
		builder.withMinorAlarm(5l);
		builder.withMajorAlarm(6l);
		builder.withEmergencySwitchStatus(EmergencySwitchStatusEnum.ON);
		builder.withCurrentBattery(9l);
		builder.withTempBattery1(BigInteger.valueOf(3));
		builder.withTempBattery2(BigInteger.valueOf(3));
		builder.withTempBattery3(BigInteger.valueOf(3));
		builder.withTempBatteryMin(BigInteger.valueOf(5));
		builder.withTempBatteryMax(BigInteger.valueOf(10));
		builder.withTempBatteryAvg(BigInteger.valueOf(3));
		builder.withBattSoC(BigInteger.valueOf(3));
		builder.withEnergyHourLast(BigInteger.valueOf(3));
		builder.withEnergyDayLast(BigInteger.valueOf(3));
		builder.withEnergyMonthLast(BigInteger.valueOf(9));
    	PcsInverter pcsInverter = new PcsInverter(builder);
    	pcsInverterServiceImpl.dispatch(pcsInverter);
	
		
	}

	
}
