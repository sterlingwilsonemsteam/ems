/**
 * 
 */
package com.sw.ems.cloud.service.dataingestor.impl;

import org.springframework.stereotype.Repository;

import com.sw.ems.cloud.service.dataingestor.api.WorkUnitService;
import com.sw.ems.common.model.coredomain.impl.device.Device;

/**
 * @author hema
 *
 */
@Repository
public class WorkUnitServiceImpl extends StandardDataIngestorService<Device> implements WorkUnitService {

}


