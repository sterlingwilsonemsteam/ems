/**
 * 
 */
package com.sw.ems.cloud.service.dataingestor.api;

import javax.ejb.Remote;

import com.sw.ems.common.model.coredomain.impl.device.Device;

/**
 * @author hema
 *
 */
@Remote
public interface WorkUnitService extends IDataIngestorService<Device>{
	
}
