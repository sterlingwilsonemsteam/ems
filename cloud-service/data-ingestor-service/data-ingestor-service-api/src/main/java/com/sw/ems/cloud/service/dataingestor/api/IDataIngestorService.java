package com.sw.ems.cloud.service.dataingestor.api;

/**
 *
 */
public interface IDataIngestorService<T> {
	 public T dispatch(T t);
}
