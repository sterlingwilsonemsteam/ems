/**
 * 
 */
package com.sw.ems.cloud.service.dataingestor.api;

import com.sw.ems.common.model.coredomain.impl.device.Device;
import javax.ejb.Remote;
/**
 * @author hema
 *
 */
@Remote
public interface DispatcherService extends IDataIngestorService<Device>{
	
}
