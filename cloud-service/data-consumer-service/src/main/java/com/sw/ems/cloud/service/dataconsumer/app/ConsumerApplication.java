package com.sw.ems.cloud.service.dataconsumer.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * @author Iqbal
 *
 */
@SpringBootApplication
@ComponentScan("com.sw.ems.cloud.service.dataconsumer.* , com.sw.ems.cloud.service.persistence.*")
public class ConsumerApplication {

	public static void main(String[] args) throws JsonProcessingException {
		SpringApplication.run(ConsumerApplication.class, args);
	}
}
