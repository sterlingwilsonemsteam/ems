package com.sw.cloud.service.dataconsumer.api;

import com.sw.ems.common.model.coredomain.impl.PcsInverter;

public interface IDataConsumerService {

	public void onDataConsumed(PcsInverter inverter);
}
