package com.sw.cloud.service.dataconsumer.api.utils;

public class Constant {
	public static final String EMS_GLOBAL_TOPIC = "ems_global_topic";
	public static final String EMS_PCS_INVERTER_TOPIC = "ems_pcs_inverter_topic";
 }
