package com.sw.cloud.service.dataconsumer.api.model;

import java.math.BigInteger;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @author Iqbal
 *
 */

@AllArgsConstructor
@NoArgsConstructor
public class PCSInverterDTO {

	@JsonProperty("id")
	private Long id;

	@JsonProperty("deviceId")
	private String deviceId;

	@JsonProperty("serialNumber")
	@JsonIgnoreProperties(ignoreUnknown = true)
	private String serialNumber;

	@JsonProperty("manufacturer")
	private String manufacturer;

	@JsonProperty("modelNumber")
	private String modelNumber;

	@JsonProperty("fwVersion")
	@JsonIgnoreProperties(ignoreUnknown = true)
	private String fwVersion;

	@JsonProperty("pvInverterType")
	private String pvInverterType;

	@JsonProperty("size")
	private String size;

	@JsonProperty("powerOutputAC")
	private BigInteger powerOutputAC;

	@JsonProperty("pcsVoltageAC")
	private BigInteger pcsVoltageAC;

	@JsonProperty("pcsCurrentAC")
	private BigInteger pcsCurrentAC;

	@JsonProperty("powerActiveAC")
	@JsonIgnoreProperties(ignoreUnknown = true)
	private BigInteger powerActiveAC;

	@JsonProperty("powerReactiveAC")
	private BigInteger powerReactiveAC;

	@JsonProperty("powerApparentAC")
	@JsonIgnoreProperties(ignoreUnknown = true)
	private BigInteger powerApparentAC;

	@JsonProperty("dcBusVoltage")
	private BigInteger dcBusVoltage;

	@JsonProperty("frequencyGrid")
	private BigInteger frequencyGrid;

	@JsonProperty("pcsStatus")
	@JsonIgnoreProperties(ignoreUnknown = true)
	private Long pcsStatus;

	@JsonProperty("runningStatus")
	private String runningStatus;

	@JsonProperty("powerBattery")
	private BigInteger powerBattery;

	@JsonProperty("watchdogTimer")
	@JsonIgnoreProperties(ignoreUnknown = true)
	private Long watchdogTimer;

	@JsonProperty("minorAlarm")
	private Long minorAlarm;

	@JsonProperty("majorAlarm")
	private Long majorAlarm;

	@JsonProperty("emergencySwitchStatus")
	private String emergencySwitchStatus;

	@JsonProperty("CurrentBattery")
	@JsonIgnoreProperties(ignoreUnknown = true)
	private Long CurrentBattery;

	@JsonProperty("tempBattery1")
	@JsonIgnoreProperties(ignoreUnknown = true)
	private BigInteger tempBattery1;

	@JsonProperty("tempBattery2")
	@JsonIgnoreProperties(ignoreUnknown = true)
	private BigInteger tempBattery2;

	@JsonProperty("tempBattery3")
	@JsonIgnoreProperties(ignoreUnknown = true)
	private BigInteger tempBattery3;

	@JsonProperty("tempBatteryMin")
	@JsonIgnoreProperties(ignoreUnknown = true)
	private BigInteger tempBatteryMin;

	@JsonProperty("tempBatteryMax")
	@JsonIgnoreProperties(ignoreUnknown = true)
	private BigInteger tempBatteryMax;

	@JsonProperty("tempBatteryAvg")
	@JsonIgnoreProperties(ignoreUnknown = true)
	private BigInteger tempBatteryAvg;

	@JsonProperty("battSoC")
	@JsonIgnoreProperties(ignoreUnknown = true)
	private BigInteger battSoC;

	@JsonProperty("energyHourLast")
	@JsonIgnoreProperties(ignoreUnknown = true)
	private BigInteger energyHourLast;

	@JsonProperty("energyDayLast")
	@JsonIgnoreProperties(ignoreUnknown = true)
	private BigInteger energyDayLast;

	@JsonProperty("energyMonthLast")
	@JsonIgnoreProperties(ignoreUnknown = true)
	private BigInteger energyMonthLast;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the deviceId
	 */
	public String getDeviceId() {
		return deviceId;
	}

	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	/**
	 * @return the serialNumber
	 */
	public String getSerialNumber() {
		return serialNumber;
	}

	/**
	 * @param serialNumber the serialNumber to set
	 */
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	/**
	 * @return the manufacturer
	 */
	public String getManufacturer() {
		return manufacturer;
	}

	/**
	 * @param manufacturer the manufacturer to set
	 */
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	/**
	 * @return the modelNumber
	 */
	public String getModelNumber() {
		return modelNumber;
	}

	/**
	 * @param modelNumber the modelNumber to set
	 */
	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	/**
	 * @return the fwVersion
	 */
	public String getFwVersion() {
		return fwVersion;
	}

	/**
	 * @param fwVersion the fwVersion to set
	 */
	public void setFwVersion(String fwVersion) {
		this.fwVersion = fwVersion;
	}

	/**
	 * @return the pvInverterType
	 */
	public String getPvInverterType() {
		return pvInverterType;
	}

	/**
	 * @param pvInverterType the pvInverterType to set
	 */
	public void setPvInverterType(String pvInverterType) {
		this.pvInverterType = pvInverterType;
	}

	/**
	 * @return the size
	 */
	public String getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(String size) {
		this.size = size;
	}

	/**
	 * @return the powerOutputAC
	 */
	public BigInteger getPowerOutputAC() {
		return powerOutputAC;
	}

	/**
	 * @param powerOutputAC the powerOutputAC to set
	 */
	public void setPowerOutputAC(BigInteger powerOutputAC) {
		this.powerOutputAC = powerOutputAC;
	}

	/**
	 * @return the pcsVoltageAC
	 */
	public BigInteger getPcsVoltageAC() {
		return pcsVoltageAC;
	}

	/**
	 * @param pcsVoltageAC the pcsVoltageAC to set
	 */
	public void setPcsVoltageAC(BigInteger pcsVoltageAC) {
		this.pcsVoltageAC = pcsVoltageAC;
	}

	/**
	 * @return the pcsCurrentAC
	 */
	public BigInteger getPcsCurrentAC() {
		return pcsCurrentAC;
	}

	/**
	 * @param pcsCurrentAC the pcsCurrentAC to set
	 */
	public void setPcsCurrentAC(BigInteger pcsCurrentAC) {
		this.pcsCurrentAC = pcsCurrentAC;
	}

	/**
	 * @return the powerActiveAC
	 */
	public BigInteger getPowerActiveAC() {
		return powerActiveAC;
	}

	/**
	 * @param powerActiveAC the powerActiveAC to set
	 */
	public void setPowerActiveAC(BigInteger powerActiveAC) {
		this.powerActiveAC = powerActiveAC;
	}

	/**
	 * @return the powerReactiveAC
	 */
	public BigInteger getPowerReactiveAC() {
		return powerReactiveAC;
	}

	/**
	 * @param powerReactiveAC the powerReactiveAC to set
	 */
	public void setPowerReactiveAC(BigInteger powerReactiveAC) {
		this.powerReactiveAC = powerReactiveAC;
	}

	/**
	 * @return the powerApparentAC
	 */
	public BigInteger getPowerApparentAC() {
		return powerApparentAC;
	}

	/**
	 * @param powerApparentAC the powerApparentAC to set
	 */
	public void setPowerApparentAC(BigInteger powerApparentAC) {
		this.powerApparentAC = powerApparentAC;
	}

	/**
	 * @return the dcBusVoltage
	 */
	public BigInteger getDcBusVoltage() {
		return dcBusVoltage;
	}

	/**
	 * @param dcBusVoltage the dcBusVoltage to set
	 */
	public void setDcBusVoltage(BigInteger dcBusVoltage) {
		this.dcBusVoltage = dcBusVoltage;
	}

	/**
	 * @return the frequencyGrid
	 */
	public BigInteger getFrequencyGrid() {
		return frequencyGrid;
	}

	/**
	 * @param frequencyGrid the frequencyGrid to set
	 */
	public void setFrequencyGrid(BigInteger frequencyGrid) {
		this.frequencyGrid = frequencyGrid;
	}

	/**
	 * @return the pcsStatus
	 */
	public Long getPcsStatus() {
		return pcsStatus;
	}

	/**
	 * @param pcsStatus the pcsStatus to set
	 */
	public void setPcsStatus(Long pcsStatus) {
		this.pcsStatus = pcsStatus;
	}

	/**
	 * @return the runningStatus
	 */
	public String getRunningStatus() {
		return runningStatus;
	}

	/**
	 * @param runningStatus the runningStatus to set
	 */
	public void setRunningStatus(String runningStatus) {
		this.runningStatus = runningStatus;
	}

	/**
	 * @return the powerBattery
	 */
	public BigInteger getPowerBattery() {
		return powerBattery;
	}

	/**
	 * @param powerBattery the powerBattery to set
	 */
	public void setPowerBattery(BigInteger powerBattery) {
		this.powerBattery = powerBattery;
	}

	/**
	 * @return the watchdogTimer
	 */
	public Long getWatchdogTimer() {
		return watchdogTimer;
	}

	/**
	 * @param watchdogTimer the watchdogTimer to set
	 */
	public void setWatchdogTimer(Long watchdogTimer) {
		this.watchdogTimer = watchdogTimer;
	}

	/**
	 * @return the minorAlarm
	 */
	public Long getMinorAlarm() {
		return minorAlarm;
	}

	/**
	 * @param minorAlarm the minorAlarm to set
	 */
	public void setMinorAlarm(Long minorAlarm) {
		this.minorAlarm = minorAlarm;
	}

	/**
	 * @return the majorAlarm
	 */
	public Long getMajorAlarm() {
		return majorAlarm;
	}

	/**
	 * @param majorAlarm the majorAlarm to set
	 */
	public void setMajorAlarm(Long majorAlarm) {
		this.majorAlarm = majorAlarm;
	}

	/**
	 * @return the emergencySwitchStatus
	 */
	public String getEmergencySwitchStatus() {
		return emergencySwitchStatus;
	}

	/**
	 * @param emergencySwitchStatus the emergencySwitchStatus to set
	 */
	public void setEmergencySwitchStatus(String emergencySwitchStatus) {
		this.emergencySwitchStatus = emergencySwitchStatus;
	}

	/**
	 * @return the currentBattery
	 */
	public Long getCurrentBattery() {
		return CurrentBattery;
	}

	/**
	 * @param currentBattery the currentBattery to set
	 */
	public void setCurrentBattery(Long currentBattery) {
		CurrentBattery = currentBattery;
	}

	/**
	 * @return the tempBattery1
	 */
	public BigInteger getTempBattery1() {
		return tempBattery1;
	}

	/**
	 * @param tempBattery1 the tempBattery1 to set
	 */
	public void setTempBattery1(BigInteger tempBattery1) {
		this.tempBattery1 = tempBattery1;
	}

	/**
	 * @return the tempBattery2
	 */
	public BigInteger getTempBattery2() {
		return tempBattery2;
	}

	/**
	 * @param tempBattery2 the tempBattery2 to set
	 */
	public void setTempBattery2(BigInteger tempBattery2) {
		this.tempBattery2 = tempBattery2;
	}

	/**
	 * @return the tempBattery3
	 */
	public BigInteger getTempBattery3() {
		return tempBattery3;
	}

	/**
	 * @param tempBattery3 the tempBattery3 to set
	 */
	public void setTempBattery3(BigInteger tempBattery3) {
		this.tempBattery3 = tempBattery3;
	}

	/**
	 * @return the tempBatteryMin
	 */
	public BigInteger getTempBatteryMin() {
		return tempBatteryMin;
	}

	/**
	 * @param tempBatteryMin the tempBatteryMin to set
	 */
	public void setTempBatteryMin(BigInteger tempBatteryMin) {
		this.tempBatteryMin = tempBatteryMin;
	}

	/**
	 * @return the tempBatteryMax
	 */
	public BigInteger getTempBatteryMax() {
		return tempBatteryMax;
	}

	/**
	 * @param tempBatteryMax the tempBatteryMax to set
	 */
	public void setTempBatteryMax(BigInteger tempBatteryMax) {
		this.tempBatteryMax = tempBatteryMax;
	}

	/**
	 * @return the tempBatteryAvg
	 */
	public BigInteger getTempBatteryAvg() {
		return tempBatteryAvg;
	}

	/**
	 * @param tempBatteryAvg the tempBatteryAvg to set
	 */
	public void setTempBatteryAvg(BigInteger tempBatteryAvg) {
		this.tempBatteryAvg = tempBatteryAvg;
	}

	/**
	 * @return the battSoC
	 */
	public BigInteger getBattSoC() {
		return battSoC;
	}

	/**
	 * @param battSoC the battSoC to set
	 */
	public void setBattSoC(BigInteger battSoC) {
		this.battSoC = battSoC;
	}

	/**
	 * @return the energyHourLast
	 */
	public BigInteger getEnergyHourLast() {
		return energyHourLast;
	}

	/**
	 * @param energyHourLast the energyHourLast to set
	 */
	public void setEnergyHourLast(BigInteger energyHourLast) {
		this.energyHourLast = energyHourLast;
	}

	/**
	 * @return the energyDayLast
	 */
	public BigInteger getEnergyDayLast() {
		return energyDayLast;
	}

	/**
	 * @param energyDayLast the energyDayLast to set
	 */
	public void setEnergyDayLast(BigInteger energyDayLast) {
		this.energyDayLast = energyDayLast;
	}

	/**
	 * @return the energyMonthLast
	 */
	public BigInteger getEnergyMonthLast() {
		return energyMonthLast;
	}

	/**
	 * @param energyMonthLast the energyMonthLast to set
	 */
	public void setEnergyMonthLast(BigInteger energyMonthLast) {
		this.energyMonthLast = energyMonthLast;
	}
	
	

}
