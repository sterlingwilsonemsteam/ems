package com.sw.ems.cloud.service.dataconsumer.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sw.cloud.service.dataconsumer.api.IDataConsumerService;
import com.sw.ems.cloud.service.persistence.api.IPcsInverterDAO;
import com.sw.ems.cloud.service.persistence.api.exception.PersistenceServiceException;
import com.sw.ems.common.model.coredomain.impl.device.PcsInverter;

/**
 * @author Iqbal
 *
 */
@Service
public class StandardDataConsumerService implements IDataConsumerService {

	@Autowired
	private IPcsInverterDAO iPcsInverterDAO;
 
	/** 
	 * Inside this method we will call persistence layer to persist the PCSInverter
	 * object
	 */
	@Override
	public void onDataConsumed(PcsInverter inverter) {

		System.out.println("id is :" + inverter.getId());
		if (inverter != null)
			try {
				iPcsInverterDAO.createPcsInverter(inverter);
			} catch (IOException | PersistenceServiceException e) {
				e.printStackTrace();
			}

	}

}
