package com.sw.ems.cloud.service.dataconsumer.impl.configurations;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author iqbal
 * 
 * This class will be responsible for providing required beans
 */

@Configuration
public class BeanConfigurations {

	@Bean
	public ModelMapper modelMapperBean() {
		return new ModelMapper();
	}

}
