package com.sw.ems.cloud.service.dataconsumer.impl.util;

import com.sw.cloud.service.dataconsumer.api.model.PCSInverterDTO;
import com.sw.ems.common.model.coredomain.impl.device.PcsInverter;
import com.sw.ems.common.model.coredomain.impl.device.PcsInverter.PcsInverterBuilder;

/**
 * This Class is responsible for doing model mapping , this can be replaced by using ModleMapper library.
 * Currently Custom builder pattern has bug for Modelmapping  {@link Link}
 * @see href https://github.com/modelmapper/modelmapper/issues/265 
 * @author iqbal
 * 
 *
 */
public class EmsObjectMapper {

	public static PcsInverter map(PCSInverterDTO pcsInverterDTO) {
		return new PcsInverterBuilder().withId(pcsInverterDTO.getId() != null ? pcsInverterDTO.getId() : null)
				.withDeviceId(pcsInverterDTO.getDeviceId() != null ? pcsInverterDTO.getDeviceId() : null)
				.withSerialNumber(pcsInverterDTO.getSerialNumber() != null ? pcsInverterDTO.getSerialNumber() : null)
				.withManufacturer(pcsInverterDTO.getManufacturer() != null ? pcsInverterDTO.getManufacturer() : null)
				.withModelNumber(pcsInverterDTO.getModelNumber() != null ? pcsInverterDTO.getModelNumber() : null)
				.withFwVersion(pcsInverterDTO.getFwVersion() != null ? pcsInverterDTO.getFwVersion() : null)
				// .withPvInverterType(pcsInverterDTO.getPvInverterType() !=null ?
				// pcsInverterDTO.getPvInverterType() : null)
				.withSize(pcsInverterDTO.getSize() != null ? pcsInverterDTO.getSize() : null)
				.withPowerOutputAC(pcsInverterDTO.getPowerOutputAC() != null ? pcsInverterDTO.getPowerOutputAC() : null)
				.withPcsVoltageAC(pcsInverterDTO.getPcsVoltageAC() != null ? pcsInverterDTO.getPcsVoltageAC() : null)
				.withPcsCurrentAC(pcsInverterDTO.getPcsCurrentAC() != null ? pcsInverterDTO.getPcsCurrentAC() : null)
				.withPowerActiveAC(pcsInverterDTO.getPowerActiveAC() != null ? pcsInverterDTO.getPowerActiveAC() : null)
				.withPowerReactiveAC(
						pcsInverterDTO.getPowerReactiveAC() != null ? pcsInverterDTO.getPowerReactiveAC() : null)
				.withPowerApparentAC(
						pcsInverterDTO.getPowerApparentAC() != null ? pcsInverterDTO.getPowerApparentAC() : null)
				.withDcBusVoltage(pcsInverterDTO.getDcBusVoltage() != null ? pcsInverterDTO.getDcBusVoltage() : null)
				.withFrequencyGrid(pcsInverterDTO.getFrequencyGrid() != null ? pcsInverterDTO.getFrequencyGrid() : null)
				.withPcsStatus(pcsInverterDTO.getPcsStatus() != null ? pcsInverterDTO.getPcsStatus() : null)
				// .withRunningStatus(pcsInverterDTO.getRunningStatus() !=null ?
				// pcsInverterDTO.getRunningStatus() : null)
				.withPowerBattery(pcsInverterDTO.getPowerBattery() != null ? pcsInverterDTO.getPowerBattery() : null)
				.withWatchdogTimer(pcsInverterDTO.getWatchdogTimer() != null ? pcsInverterDTO.getWatchdogTimer() : null)
				.withMinorAlarm(pcsInverterDTO.getMinorAlarm() != null ? pcsInverterDTO.getMinorAlarm() : null)
				.withMajorAlarm(pcsInverterDTO.getMajorAlarm() != null ? pcsInverterDTO.getMajorAlarm() : null)
				//.withEmergencySwitchStatus(
					//	pcsInverterDTO.getEmergencySwitchStatus() != null ? pcsInverterDTO.getEmergencySwitchStatus()
						//		: null)
				.withCurrentBattery(
						pcsInverterDTO.getCurrentBattery() != null ? pcsInverterDTO.getCurrentBattery() : null)
				.withTempBattery1(pcsInverterDTO.getTempBattery1() != null ? pcsInverterDTO.getTempBattery1() : null)
				.withTempBattery2(pcsInverterDTO.getTempBattery2() != null ? pcsInverterDTO.getTempBattery2() : null)
				.withTempBattery3(pcsInverterDTO.getTempBattery3() != null ? pcsInverterDTO.getTempBattery3() : null)
				.withTempBatteryMin(
						pcsInverterDTO.getTempBatteryMin() != null ? pcsInverterDTO.getTempBatteryMin() : null)
				.withTempBatteryMax(
						pcsInverterDTO.getTempBatteryMax() != null ? pcsInverterDTO.getTempBatteryMax() : null)
				.withTempBatteryAvg(
						pcsInverterDTO.getTempBatteryAvg() != null ? pcsInverterDTO.getTempBatteryAvg() : null)
				.withBattSoC(pcsInverterDTO.getBattSoC() != null ? pcsInverterDTO.getBattSoC() : null)
				.withEnergyHourLast(
						pcsInverterDTO.getEnergyHourLast() != null ? pcsInverterDTO.getEnergyHourLast() : null)
				.withEnergyDayLast(pcsInverterDTO.getEnergyDayLast() != null ? pcsInverterDTO.getEnergyDayLast() : null)
				.withEnergyMonthLast(
						pcsInverterDTO.getEnergyMonthLast() != null ? pcsInverterDTO.getEnergyMonthLast() : null)
				.build();
	}

}
