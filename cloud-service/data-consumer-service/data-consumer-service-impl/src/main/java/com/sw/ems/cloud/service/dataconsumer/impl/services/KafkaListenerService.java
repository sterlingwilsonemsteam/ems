package com.sw.ems.cloud.service.dataconsumer.impl.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sw.cloud.service.dataconsumer.api.IDataConsumerService;
import com.sw.cloud.service.dataconsumer.api.utils.Constant;
import com.sw.ems.common.model.coredomain.impl.device.PcsInverter;

/**
 * @author iqbal
 *
 */

@Service
public class KafkaListenerService {

	@Autowired
	private IDataConsumerService dataConsumerService;

	/**
	 * This Method is called whenever there is some message published to kafka topic
	 * 
	 * @param pcsInverterDTO
	 * @throws JsonProcessingException
	 */
	@KafkaListener(topics = Constant.EMS_PCS_INVERTER_TOPIC, containerFactory = "pcsInverterDTOKafkaListenerFactory")
	public void consumeJson(PcsInverter inverter ) throws JsonProcessingException {
		dataConsumerService.onDataConsumed(inverter);
	}
}
