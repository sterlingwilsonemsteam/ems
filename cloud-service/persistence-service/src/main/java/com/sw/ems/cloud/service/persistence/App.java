package com.sw.ems.cloud.service.persistence;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import com.sw.ems.cloud.service.persistence.api.IPcsInverterDAO;
import com.sw.ems.cloud.service.persistence.api.exception.PersistenceServiceException;
import com.sw.ems.cloud.service.persistence.api.exception.PropertyFileException;
import com.sw.ems.cloud.service.persistence.util.MongoDb;
import com.sw.ems.common.model.coredomain.impl.device.PcsInverter;

public class App {
	public static void main(String[] args) throws IOException, PropertyFileException {

		AbstractApplicationContext context = new AnnotationConfigApplicationContext(MongoDb.class);
		IPcsInverterDAO inverterDao = context.getBean(IPcsInverterDAO.class);
		// inverterDao.updatePcsInverter(25L);
		// inverterDao.findPcsInverterById(25L);
		// inverterDao.createPcsInverter(getPcsInverter());
		 //inverterDao.deleteAllPcsInverter();

		//inverterDao.createBulkPcsInverter(getPcsInverterList());

		try {
			Collection<PcsInverter> collection = inverterDao.getAllPcsInverters();
		} catch (PersistenceServiceException e) {
			e.printStackTrace();
		}

		context.close();
	}

	
	  public static List<PcsInverter> getPcsInverterList() { List<PcsInverter>
	  pcsInverterList = new ArrayList<PcsInverter>(); PcsInverter inverter = null;
	  for (int i = 0; i < 3; i++) { inverter = new
	  PcsInverter.PcsInverterBuilder().withCurrentBattery(10L + i).build();
	  pcsInverterList.add(inverter); }
	  
	  return pcsInverterList; }
	  
	  public static PcsInverter getPcsInverter() { return new
	  PcsInverter.PcsInverterBuilder().withCurrentBattery(12L).withDeviceId(
	  "testdeviceID"). withEnergyHourLast(new BigInteger(1900+"")).build(); }
	 

}
