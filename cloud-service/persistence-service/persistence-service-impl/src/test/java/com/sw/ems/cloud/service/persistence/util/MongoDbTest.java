package com.sw.ems.cloud.service.persistence.util;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.sw.ems.cloud.service.persistence.api.exception.PersistenceServiceException;

/**
 * The mongo DB test.
 * 
 * @author Gaurav Garg
 * @version 1.0
 * @since 2019-11-30
 * 
 */
public class MongoDbTest {

	@Test()
	public void testMongoClientHappyCase() throws PersistenceServiceException {
		
		assertThat(new MongoDb().getMongoClient(), is(notNullValue()));
		assertThat(new MongoDb().getMongoClient(), instanceOf(MongoClient.class));
	}

	@Test()
	public void testMongoDbNameHappyCase() throws PersistenceServiceException {
		
		assertThat(new MongoDb().getMongoDbName(), is(notNullValue()));
		assertThat(new MongoDb().getMongoDbName(), instanceOf(MongoDatabase.class));
	}

	@Test()
	public void testMongoCollectionHappyCase() throws PersistenceServiceException {
		
		assertThat(new MongoDb().getMongoCollection(), is(notNullValue()));
		assertThat(new MongoDb().getMongoCollection(), instanceOf(MongoCollection.class));
	}


}
