package com.sw.ems.cloud.service.persistence.util;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.sw.ems.cloud.service.persistence.api.exception.PropertyFileException;

public class PropertyFileUtilTest {

	@Rule
	public ExpectedException exceptionRule = ExpectedException.none();

	@Test()
	public void testReadPropertiesHappyCase() throws PropertyFileException {
		exceptionRule.expect(PropertyFileException.class);
		exceptionRule.expectMessage("File Name is empty");
		//PropertyFileUtil.readProperties(StringUtils.EMPTY);
	}

	@Test()
	public void testReadPropertiesNotHappyCase() throws PropertyFileException {
		//assertThat(PropertyFileUtil.readProperties(CONFIG_PROPERTY), is(notNullValue()));
		//assertThat(PropertyFileUtil.readProperties(CONFIG_PROPERTY), instanceOf(Properties.class));
	}

}
