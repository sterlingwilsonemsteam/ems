package com.sw.ems.cloud.service.persistence.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import com.sw.ems.cloud.service.persistence.api.exception.ErrorCode;
import com.sw.ems.cloud.service.persistence.api.exception.PropertyFileException;

/** 
 * This is utility class used for reading property file.
 *    
 * @author Gaurav Garg
 * @version 1.0
 * @since 2019-11-30
 *
 */
@Configuration
public class PropertyFileUtil {

	
	private static Logger logger = LoggerFactory.getLogger(MongoDb.class);

	/**
	 * Get the property from property file based on file name.
	 * 
	 * @param fileName
	 * @return The property object with loaded key/value pairs.
	 * @throws PropertyFileException 
	 */

	public Properties readProperties(String fileName) throws PropertyFileException {
		logger.debug("inside readProperties method");
		Properties prop = new Properties();
		// using java 8 try-with-resources structure
		// so the input stream will be closed automatically
		if (StringUtils.isNotEmpty(fileName)) {
			try (InputStream inputStream = PropertyFileUtil.class.getClassLoader().getResourceAsStream(fileName)) {

				// load the properties
				prop.load(inputStream);
			} catch (IOException e) {
				logger.error("error reading property file",e);
				throw new PropertyFileException(e,ErrorCode.FILE_READ_ERROR);
			}

		}else {
			logger.error("File Name = {} is empty", fileName);
		 	throw new PropertyFileException("File Name is empty", ErrorCode.EMPTY_FILE_NAME);
		}
		logger.debug("Properties object = {} based on fileName = {}" ,prop , fileName);
		logger.debug("end readProperties method");
		return prop;
	}

}
