package com.sw.ems.cloud.service.persistence.util;

import static com.sw.ems.cloud.service.persistence.util.Constants.COLLECTION_PCS_INVERTER;
import static com.sw.ems.cloud.service.persistence.util.Constants.CONFIG_PROPERTY;
import static com.sw.ems.cloud.service.persistence.util.Constants.MONGO_DB_CONNECTION_PER_HOST;
import static com.sw.ems.cloud.service.persistence.util.Constants.MONGO_DB_CONNECTION_TIME_OUT;
import static com.sw.ems.cloud.service.persistence.util.Constants.MONGO_DB_HEARTBEAT_CONNECTION_TIME_OUT;
import static com.sw.ems.cloud.service.persistence.util.Constants.MONGO_DB_MAX_CONNECTION_IDLE_TIME;
import static com.sw.ems.cloud.service.persistence.util.Constants.MONGO_DB_MAX_CONNECTION_LIFE_TIME;
import static com.sw.ems.cloud.service.persistence.util.Constants.MONGO_DB_MAX_WAIT_TIME;
import static com.sw.ems.cloud.service.persistence.util.Constants.MONGO_DB_NAME;
import static com.sw.ems.cloud.service.persistence.util.Constants.MONGO_DB_SOCKET_TIMEOUT;
import static com.sw.ems.cloud.service.persistence.util.Constants.MONGO_URI;

import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.sw.ems.cloud.service.persistence.api.exception.ErrorCode;
import com.sw.ems.cloud.service.persistence.api.exception.PersistenceServiceException;
import com.sw.ems.cloud.service.persistence.api.exception.PropertyFileException;

/**
 * This class is used for defining all mongo db operation and configuration.  
 * @author Gaurav Garg
 * @version 1.0
 * @since 2019-11-30
 */
@Configuration
@ComponentScan("com.sw.ems.cloud.service.persistence")
public class MongoDb {

	private static Logger logger = LoggerFactory.getLogger(MongoDb.class);
	private static MongoDb instance = new MongoDb();

	@Autowired
	private Properties prop;

	private MongoClient mongo;

	@Autowired
	private PropertyFileUtil fileUtil;

	/**
	 * @return the MongoClient
	 */
	@Bean
	public MongoClient getMongoClient() {
		logger.debug("inside getMongoClient method");
		prop = getConfigFileProperties();

		if (mongo == null) {
			logger.debug("Starting Mongo");
			if (null != prop) {
				MongoClientOptions.Builder options = MongoClientOptions.builder()
					.connectionsPerHost(StringUtils.isNotEmpty(prop.getProperty(MONGO_DB_CONNECTION_PER_HOST))? 
							Integer.parseInt(prop.getProperty(MONGO_DB_CONNECTION_PER_HOST)): 0)
					.connectTimeout(StringUtils.isNotEmpty(prop.getProperty(MONGO_DB_CONNECTION_TIME_OUT))? 
							Integer.parseInt(prop.getProperty(MONGO_DB_CONNECTION_TIME_OUT)): 0)
					.maxConnectionIdleTime(StringUtils.isNotEmpty(prop.getProperty(MONGO_DB_MAX_CONNECTION_IDLE_TIME))? 
							Integer.parseInt(prop.getProperty(MONGO_DB_MAX_CONNECTION_IDLE_TIME)): 0)
					.maxConnectionLifeTime(StringUtils.isNotEmpty(prop.getProperty(MONGO_DB_MAX_CONNECTION_LIFE_TIME))? 
							Integer.parseInt(prop.getProperty(MONGO_DB_MAX_CONNECTION_LIFE_TIME)): 0)
					.socketTimeout(StringUtils.isNotEmpty(prop.getProperty(MONGO_DB_SOCKET_TIMEOUT))? 
							Integer.parseInt(prop.getProperty(MONGO_DB_SOCKET_TIMEOUT)): 0)
					.maxWaitTime(StringUtils.isNotEmpty(prop.getProperty(MONGO_DB_MAX_WAIT_TIME))? 
							Integer.parseInt(prop.getProperty(MONGO_DB_MAX_WAIT_TIME)): 0)
					.heartbeatConnectTimeout(StringUtils.isNotEmpty(prop.getProperty(MONGO_DB_HEARTBEAT_CONNECTION_TIME_OUT))? 
							Integer.parseInt(prop.getProperty(MONGO_DB_HEARTBEAT_CONNECTION_TIME_OUT)): 0);

				MongoClientURI uri = new MongoClientURI(prop.getProperty(MONGO_URI), options);

				if (logger.isDebugEnabled()) {
					logger.debug("mongo db connection parameters = {}", uri.getOptions());
				}

				logger.debug("About to connect to MongoDB @ " + uri.toString());

				try {
					mongo = new MongoClient(uri);
				} catch (MongoException ex) {
					logger.error("An error occoured when connecting to MongoDB", ex);
				} catch (Exception ex) {
					logger.error("An error occoured when connecting to MongoDB", ex);
				}
			}

		}
		logger.debug("end getMongoClient method");
		return mongo;
	}

	/**
	 * @return the Properties
	 */
	public Properties getConfigFileProperties() {
		logger.debug("inside getConfigFileProperties method");
		try {
			prop = fileUtil.readProperties(CONFIG_PROPERTY);
		} catch (PropertyFileException e) {
			logger.error("error reading config property file ", e);
			e.printStackTrace();
		}
		logger.debug("end getConfigFileProperties method");
		return prop;
	}

	/**
	 * @return the MongoDatabase
	 * @throws MongoDbException
	 */
	public MongoDatabase getMongoDbName() throws PersistenceServiceException{
		logger.debug("inside getConfigFileProperties method");
		MongoDatabase db = null;
		if(null!= prop) {
			db = getMongoClient().getDatabase(prop.getProperty(MONGO_DB_NAME));	
		}else {
			logger.error("property object = {} is null" , prop);
			throw new PersistenceServiceException("property object is null",ErrorCode.FILE_READ_ERROR);
		}
		logger.debug("inside getConfigFileProperties method");
		return db;
	}

	/**
	 * @return the MongoCollection<Document>
	 * @throws PersistenceServiceException
	 */
	public MongoCollection<Document> getMongoCollection() throws PersistenceServiceException{
		logger.debug("start getMongoCollection method");
		MongoCollection<Document> collection = null;
		try {
			if(null!= prop) {
				collection = getMongoDbName().getCollection(prop.getProperty(COLLECTION_PCS_INVERTER));	
			}else {
				throw new PersistenceServiceException("property object is null",ErrorCode.FILE_READ_ERROR);
			}
		} catch (PersistenceServiceException e) {
			logger.error("exception whiile get the mongo db name" ,e);
			e.printStackTrace();
		}
		logger.debug("end getMongoCollection method");
		return collection;
	}

	/**
	 * The close method.
	 */
	public void close() {
		logger.info("Closing MongoDB connection");
		if (mongo != null) {
			try {
				mongo.close();
				logger.debug("Nulling the connection dependency objects");
				mongo = null;
			} catch (Exception e) {
				logger.error(
						String.format("An error occurred when closing the MongoDB connection\n%s", e.getMessage()));
			}
		} else {
			logger.warn("mongo object was null, wouldn't close connection");
		}
	}

	/** 
	 * @return the MongoDb instance.
	 */
	public static MongoDb getInstance() {
		return instance;
	}

}
