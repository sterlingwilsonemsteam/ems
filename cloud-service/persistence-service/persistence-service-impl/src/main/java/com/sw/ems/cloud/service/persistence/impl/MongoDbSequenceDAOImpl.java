package com.sw.ems.cloud.service.persistence.impl;

import java.io.IOException;

import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mongodb.client.MongoCollection;
import com.sw.ems.cloud.service.persistence.api.IMongoDbSequenceDAO;
import com.sw.ems.cloud.service.persistence.api.exception.PersistenceServiceException;
import com.sw.ems.cloud.service.persistence.util.MongoDb;

/**
 * This class is used for generating the auto incremented id for mongo db sequence collection.
 *   
 * @author Gaurav Garg
 * @version 1.0
 * @since 2019-11-30
 */
@Repository
public class MongoDbSequenceDAOImpl implements IMongoDbSequenceDAO {
	
	/**
	 *  The logger.
	 */
	private static Logger logger = LoggerFactory.getLogger(MongoDbSequenceDAOImpl.class);

	@Autowired
	private MongoDb mongoConnection;
	
	/**
	 * This method will use to auto increment collection in mongo. 
	 */
	@SuppressWarnings("deprecation")
	@Override
	public Long getNextSequence(String seqKey) throws IOException, PersistenceServiceException {
		logger.debug("start getNextSequence method");
		Long seq = null;
		if(mongoConnection!= null && mongoConnection.getMongoDbName()!= null ){
			logger.debug("sequence key = {}" , seqKey);	
			MongoCollection<Document> collection = mongoConnection.getMongoDbName().getCollection(seqKey);
			
			logger.debug("DB collection for sequence = {}" , collection);
			if (collection.count() == 0) {
				seq = (Long) createCountersCollection(seqKey,collection);
			} else {
				seq = (Long) getNextValue(seqKey, collection);
			}
		}
		logger.debug("next sequence = {}" , seq);
		logger.debug("end getNextSequence method");
		return seq;
	}

	/** 
	 * This method will be use first time to create initial collection.
	 * @param seqKey
	 * @param collection
	 * @return
	 */
	public static Object createCountersCollection(String seqKey ,MongoCollection<Document> collection) {
		logger.debug("start createCountersCollection method");
		Document document = new Document();
		document.append("_id", seqKey);
		document.append("seq", 1L);
		logger.debug("counter document {}" ,document);
		collection.insertOne(document);
		logger.debug("end createCountersCollection method");
		return document.get("seq");
	}

	/** 
	 * This method will be use update initial created collection id.
	 * @param seqKey
	 * @param collection
	 * @return
	 */
	public static Object getNextValue(String seqKey, MongoCollection<Document> collection) {
		logger.debug("start getNextValue method");
		Document searchQuery = new Document("_id", seqKey);
		Document increase = new Document("seq", 1);
		Document updateQuery = new Document("$inc", increase);
		Document result = collection.findOneAndUpdate(searchQuery, updateQuery);
		
		logger.debug("incremented document {}",result);
		logger.debug("end getNextValue method");
		return result.get("seq");
	}
}
