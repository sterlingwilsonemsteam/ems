package com.sw.ems.cloud.service.persistence.impl;

import static com.sw.ems.cloud.service.persistence.util.Constants.DB_SEQUENCE;
import static com.sw.ems.cloud.service.persistence.util.Constants.FIELD_ID;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.sw.ems.cloud.service.persistence.api.IMongoDbSequenceDAO;
import com.sw.ems.cloud.service.persistence.api.IPcsInverterDAO;
import com.sw.ems.cloud.service.persistence.api.exception.PersistenceServiceException;
import com.sw.ems.cloud.service.persistence.util.MongoDb;
import com.sw.ems.common.model.coredomain.impl.device.PcsInverter;

/**
 * This interface is used for exposing the API method for another service to call persistence service.
 *   
 * @author Gaurav Garg
 * @version 1.0
 * @since 2019-11-30
 */
@Repository("iPcsInverterDAO")
public class PcsInverterDAOImpl implements IPcsInverterDAO {

	@Autowired
	private MongoDb mongoCon;

	@Autowired
	private IMongoDbSequenceDAO seqDao;

	ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	
	@Override
	public PcsInverter createPcsInverter(PcsInverter pcsInverter) throws IOException, PersistenceServiceException {
		// creating new pcsInverter object with supplied pcsInverter from kafka consumer after increment DB sequence.
		PcsInverter inverterToPersist = new PcsInverter.PcsInverterBuilder(pcsInverter).withId(seqDao.getNextSequence(DB_SEQUENCE)).build();
		
		mongoCon.getMongoCollection().insertOne(Document.parse(new Gson().toJson(inverterToPersist)));
		return inverterToPersist;
	}

	@Override
	public void createBulkPcsInverter(List<PcsInverter> pcsInverterList) throws IOException,PersistenceServiceException {
		List<Document> list = new ArrayList<>();
		for (PcsInverter pcsInverter : pcsInverterList) {
			// creating new pcsInverter object with supplied pcsInverter from kafka consumer after increment DB sequence.
			PcsInverter inverterToPersist = new PcsInverter.PcsInverterBuilder(pcsInverter).withId(seqDao.getNextSequence(DB_SEQUENCE)).build();	
			list.add(Document.parse(new Gson().toJson(inverterToPersist)));
		}
		mongoCon.getMongoCollection().insertMany(list);
	}

	@Override
	public Collection<PcsInverter> getAllPcsInverters() throws IOException, PersistenceServiceException {
		Collection<PcsInverter> pcsInverterCollection = new ArrayList<PcsInverter>();
		MongoCursor<Document> cursor = mongoCon.getMongoCollection().find().iterator();
		try {
			while (cursor.hasNext()) {
				//JSON from String to Object
				Document jsonDoc = cursor.next();
				PcsInverter pcsin = mapper.readValue(jsonDoc.toJson(), PcsInverter.class);

				System.out.println(jsonDoc.toJson());
				pcsInverterCollection.add(pcsin);
				System.out.println(pcsin);
			}
		} finally {
			cursor.close();
		}
		return pcsInverterCollection;
	}

	@Override
	public PcsInverter findPcsInverterById(Long id) throws IOException, PersistenceServiceException {
		Document document = mongoCon.getMongoCollection().find(Filters.eq(FIELD_ID, id)).first();
		PcsInverter pcsin = mapper.readValue(document.toJson(), PcsInverter.class);
		System.out.println(document.toJson());
		return pcsin;
	}

	@Override
	public Boolean updatePcsInverter(Long id) throws IOException , PersistenceServiceException{
		PcsInverter inverter = new PcsInverter.PcsInverterBuilder().withCurrentBattery(1000L).withModelNumber("testing")
				.build();
		Document tempUpdateOp = new Document("$set", Document.parse(new Gson().toJson(inverter)));
		UpdateResult result = mongoCon.getMongoCollection().updateOne(Filters.eq(FIELD_ID, id), tempUpdateOp);

		System.out.println("Document update successfully..." + result.wasAcknowledged());
		return result.wasAcknowledged();
	}

	@Override
	public Boolean deletePcsInverter(Long id) throws PersistenceServiceException{
		DeleteResult result = mongoCon.getMongoCollection().deleteOne(Filters.eq(FIELD_ID, id));
		System.out.println("Document deleted successfully..." + result.wasAcknowledged());
		return result.wasAcknowledged();
	}

	@Override
	public Boolean deleteAllPcsInverter() throws PersistenceServiceException{
		DeleteResult result = mongoCon.getMongoCollection().deleteMany(new Document());
		System.out.println("all Document deleted successfully..." + result.wasAcknowledged());
		return result.wasAcknowledged();
	}

}
