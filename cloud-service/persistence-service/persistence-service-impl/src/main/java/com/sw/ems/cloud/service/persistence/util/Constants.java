package com.sw.ems.cloud.service.persistence.util;

/**
 * The constant class
 *   
 * @author Gaurav Garg
 * @version 1.0
 * @since 2019-11-30
 */
public class Constants {

	public static final String CONFIG_PROPERTY = "config.properties";
	public static final String MONGO_DB_HOST = "mongo.db.host";
	public static final String MONGO_DB_PORT = "mongo.db.port";
	public static final String MONGO_DB_NAME = "mongo.db.name";
	public static final String COLLECTION_PCS_INVERTER = "collection.pcsinverter";
	public static final String DB_SEQUENCE = "dbsequence";
	public static final String FIELD_ID = "id";
	public static final String MONGO_URI = "mongo.uri";
	
	public static final String MONGO_DB_CONNECTION_PER_HOST = "mongo.db.connectionsPerHost";
	public static final String MONGO_DB_CONNECTION_TIME_OUT = "mongo.db.connectTimeout";
	public static final String MONGO_DB_MAX_CONNECTION_IDLE_TIME = "mongo.db.maxConnectionIdleTime";
	public static final String MONGO_DB_MAX_CONNECTION_LIFE_TIME  = "mongo.db.maxConnectionLifeTime";
	public static final String MONGO_DB_SOCKET_TIMEOUT = "mongo.db.socketTimeout";
	public static final String MONGO_DB_MAX_WAIT_TIME = "mongo.db.maxWaitTime";
	public static final String MONGO_DB_HEARTBEAT_CONNECTION_TIME_OUT  = "mongo.db.heartbeatConnectTimeout";
	
}
