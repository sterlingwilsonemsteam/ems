package com.sw.ems.cloud.service.persistence.api.exception;

/**
 * This class will used for defining the error code.  
 * @author Gaurav Garg
 * @version 1.0
 * @since 2019-08-30
 */
public enum ErrorCode {
	MONGO_DATABASE(1001, ""), 
	EMPTY_FILE_NAME(1002, "File name is empty."),
	FILE_READ_ERROR(1003, "error while file reading.");

	private final int id;
	private final String message;

	private ErrorCode(int id, String message) {
		this.id = id;
		this.message = message;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

}
