package com.sw.ems.cloud.service.persistence.api;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import com.sw.ems.cloud.service.persistence.api.exception.PersistenceServiceException;
import com.sw.ems.common.model.coredomain.impl.device.PcsInverter;

/**
 * This interface is used for exposing the api method for another service to call persistence service.
 *   
 * @author Gaurav Garg
 * @version 1.0
 * @since 2019-11-30
 */
public interface IPcsInverterDAO{

	public PcsInverter createPcsInverter(PcsInverter pcsInverter)throws IOException, PersistenceServiceException;
	
	public void createBulkPcsInverter(List<PcsInverter> pcsInverterList) throws IOException, PersistenceServiceException; 

	public Collection<PcsInverter> getAllPcsInverters() throws IOException, PersistenceServiceException ;

	public PcsInverter findPcsInverterById(Long id) throws IOException, PersistenceServiceException;

	public Boolean updatePcsInverter(Long id) throws IOException, PersistenceServiceException;

	public Boolean deletePcsInverter(Long id) throws PersistenceServiceException;

	public Boolean deleteAllPcsInverter() throws PersistenceServiceException;

}
