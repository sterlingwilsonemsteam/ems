package com.sw.ems.cloud.service.persistence.api;

import java.io.IOException;

import com.sw.ems.cloud.service.persistence.api.exception.PersistenceServiceException;

/**
 * This interface is used for generating the auto incremented id for mongo db sequence collection.  
 * @author Gaurav Garg
 * @version 1.0
 * @since 2019-11-30
 */
public interface IMongoDbSequenceDAO {
	
	/**   
	 * This method will use auto increment id for each mongo collection.
	 * @param sequenceKey
	 * @return the long generated id.
	 * @throws IOException
	 * @throws PersistenceServiceException
	 */
	public Long getNextSequence(String sequenceKey) throws IOException, PersistenceServiceException;
}
