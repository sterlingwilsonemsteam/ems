package com.sw.ems.cloud.service.persistence.api.exception;

/**
 * The PropertyFileUtilException wraps all checked standard Java exception and
 * enriches them with a custom error code. You can use this code to retrieve
 * localized error messages and to link to our online documentation.
 * 
 * @author Gaurav Garg
 * @version 1.0
 * @since 2019-11-30
 * 
 */
public class PropertyFileException extends Exception {

	/**
	 * The serial Version UID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 *  The error code.
	 */
	private ErrorCode code;

	/**
	 * @param code
	 */
	public PropertyFileException(ErrorCode code) {
		super();
		this.code = code;
	}

	/**
	 * @param message
	 * @param cause
	 * @param code
	 */
	public PropertyFileException(String message, Throwable cause, ErrorCode code) {
		super(message, cause);
		this.code = code;
	}

	/**
	 * @param message
	 * @param code
	 */
	public PropertyFileException(String message, ErrorCode code) {
		super(message);
		this.code = code;
	}

	/**
	 * @param cause
	 * @param code
	 */
	public PropertyFileException(Throwable cause, ErrorCode code) {
		super(cause);
		this.code = code;
	}

	/**
	 * @return the code
	 */
	public ErrorCode getCode() {
		return code;
	}

}
