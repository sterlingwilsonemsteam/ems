package com.sw.ems.cloud.service.persistence.api.exception;

/**
 * The PersistenceServiceException wraps all checked standard Java exception and
 * enriches them with a custom error code. You can use this code to retrieve
 * localized error messages and to link to our online documentation.
 * 
 * @author Gaurav Garg
 * @version 1.0
 * @since 2019-11-30
 */
public class PersistenceServiceException extends Exception {

	/**
	 *  The serial version.
	 */
	private static final long serialVersionUID = 1L;
	
	private ErrorCode code;
	
	/**
	 * @param code
	 */
	public PersistenceServiceException(ErrorCode code) {
		super();
		this.code = code;
	}

	/**
	 * @param message
	 * @param cause
	 * @param code
	 */
	public PersistenceServiceException(String message, Throwable cause,ErrorCode code) {
		super(message, cause);
		this.code = code;
	}

	/**
	 * @param message
	 * @param code
	 */
	public PersistenceServiceException(String message,ErrorCode code) {
		super(message);
		this.code = code;
	}

	/**
	 * @param cause
	 * @param code
	 */
	public PersistenceServiceException(Throwable cause,ErrorCode code) {
		super(cause);
		this.code = code;
	}


	/**
	 * @return the code
	 */
	public ErrorCode getCode() {
		return code;
	}
	
	
}
