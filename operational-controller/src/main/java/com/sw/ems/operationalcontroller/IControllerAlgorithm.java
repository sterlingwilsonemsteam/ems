package com.sw.ems.operationalcontroller;

/**
 * A controller algorithm that operates on asset data.
 */
public interface IControllerAlgorithm extends IAlgorithm {
}
